package com.zx.mes.hyl.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.mes.hyl.upms.entity.SysUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 根据用户名获取详细信息
     * @param name 用户名
     * @return SysUser sysUser
     */
    SysUser loadByUserName(String name);
}
