package com.zx.mes.hyl.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.mes.hyl.upms.entity.SysMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
public interface SysMenuService extends IService<SysMenu> {

}
