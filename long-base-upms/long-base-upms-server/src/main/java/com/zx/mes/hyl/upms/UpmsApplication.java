package com.zx.mes.hyl.upms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * upms 权限框架
 * @author huayunglong
 * @date 2018-7-9
 */
@MapperScan("com.zx.mes.hyl.upms.mapper")
@SpringBootApplication(scanBasePackages = {"com.zx.mes"})
@EnableDiscoveryClient
public class UpmsApplication {

    public static void main(String[]args){
        SpringApplication.run(UpmsApplication.class, args);
    }
}
