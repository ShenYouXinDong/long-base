package com.zx.mes.hyl.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.mes.hyl.upms.entity.SysUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    SysUser loadByUserName(String name);
}
