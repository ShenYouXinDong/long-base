package com.zx.mes.hyl.upms.controller;



import com.zx.mes.controller.BaseController;
import com.zx.mes.hyl.upms.entity.SysUserRole;
import com.zx.mes.hyl.upms.service.SysUserRoleService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@CrossOrigin
@RestController
@RequestMapping("/sysUserRole")
public class SysUserRoleController extends BaseController<SysUserRoleService,SysUserRole> {

}

