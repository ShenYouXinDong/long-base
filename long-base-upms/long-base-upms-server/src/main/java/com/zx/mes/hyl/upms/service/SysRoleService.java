package com.zx.mes.hyl.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.mes.hyl.upms.entity.SysRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
public interface SysRoleService extends IService<SysRole> {

}
