package com.zx.mes.hyl.upms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.mes.controller.BaseController;
import com.zx.mes.hyl.upms.entity.*;
import com.zx.mes.hyl.upms.pagemodel.TreeNode;
import com.zx.mes.hyl.upms.service.SysRoleService;
import com.zx.mes.hyl.upms.service.SysUserRoleService;
import com.zx.mes.hyl.upms.service.SysUserService;
import com.zx.mes.hyl.upms.utils.JWTHelper;
import com.zx.mes.response.ObjectRestResponse;
import com.zx.mes.response.TableResultResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <p>
 *  控制器 SysUserController
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@CrossOrigin
@RestController
@RequestMapping("/sysUser")
public class SysUserController extends BaseController<SysUserService,SysUser> {

///    private static final Logger logger = LoggerFactory.getLogger(SysUserController.class);

    private final SysUserRoleService sysUserRoleService;

    private final SysRoleService sysRoleService;

    @Autowired
    public SysUserController(SysUserRoleService sysUserRoleService, SysRoleService sysRoleService) {
        this.sysUserRoleService = sysUserRoleService;
        this.sysRoleService = sysRoleService;
    }

    @Override
    public ObjectRestResponse<SysUser> add(@RequestBody SysUser sysUser) {
        sysUser.setId(UUID.randomUUID().toString());
        sysUser.setCreateDateTime(new Date());
        return super.add(sysUser);
    }

    @Override
    public ObjectRestResponse<SysUser> update(@RequestBody SysUser sysUser) {
        sysUser.setModifyDateTime(new Date());
        return super.update(sysUser);
    }

    @Override
    public TableResultResponse<SysUser> list(@RequestParam Map<String, Object> params, SysUser sysUser) {
        int page = Integer.valueOf(params.get("page").toString());
        int limit = Integer.valueOf(params.get("limit").toString());

        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();

        if (sysUser != null && StringUtils.isNotBlank(sysUser.getName())) {
            wrapper.like("name", sysUser.getName());
        }

        if (sysUser != null && sysUser.getCreateDateTimeStart()!=null) {
            wrapper.gt("createdatetime", sysUser.getCreateDateTimeStart());
        }
        if (sysUser != null && sysUser.getCreateDatetimeEnd()!=null) {
            wrapper.lt("createdatetime", sysUser.getCreateDatetimeEnd());
        }

        List<SysUser> list = biz.page(new Page(page, limit),wrapper).getRecords();
        TableResultResponse<SysUser> response;
        if (list == null) {
            response = new TableResultResponse.Builder<SysUser>()
                    .message("无数据")
                    .page(0L)
                    .rows(null)
                    .build();
        } else {
            response = new TableResultResponse.Builder<SysUser>()
                    .message("分页查询结果")
                    .status(200)
                    .page(page)
                    .total(biz.count(wrapper))
                    .rows(list)
                    .build();
        }

        return response;

    }

    @GetMapping("/loadUserByUsername")
    public SysUser loadUserByUsername(String name){
        SysUser sysUser = new SysUser();
        return sysUser.selectOne(new QueryWrapper<SysUser>().eq("name", name));
    }

    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping("/deleteBatch")
    public ObjectRestResponse<String> deleteBatch(String ids) {
        ObjectRestResponse<String> response = new ObjectRestResponse<>();
        try {
            /// 删除sysUser表中对应的数据
            String[] idArr = ids.split(",");
            biz.removeByIds(Arrays.asList(idArr));
            /// 删除中间表sysUserRole表中对应的数据
            sysUserRoleService.remove(new QueryWrapper<SysUserRole>()
                    .in("user_id", ids));
        } catch (Exception e) {
            response.setMessage(e.getMessage());
            response.setStatus(500);
            e.printStackTrace();
        }
        response.setMessage("批量删除成功!");
        return response;
    }

    @GetMapping("/login")
    public ObjectRestResponse<Jwt> login(SysUser sysUser){
        ObjectRestResponse<Jwt> response = new ObjectRestResponse<>();
        String token;
        try {
            SysUser user = biz.getOne(
                    new QueryWrapper<SysUser>()
                            .eq("name", sysUser.getName())
                            .eq("password", sysUser.getPassword()));
            Jwt jwt = new Jwt();
            if (user != null) {
                jwt.setId(user.getId());
                jwt.setName(user.getName());
                token = JWTHelper.generateToken(jwt, "jwt/pri.key", 3600);
                jwt.setToken(token);
                response.setMessage("登录成功!");
                response.setData(jwt);
            } else {
                response.setMessage("登录失败，没有找到数据!");
                response.setStatus(500);
            }

        } catch (Exception e) {
            response.setMessage("登录失败:"+e.getMessage());
            response.setStatus(500);
            e.printStackTrace();
        }

        return response;
    }

    @GetMapping("/loadByToken")
    public ObjectRestResponse<SysUser> loadByToken(String token){
        Jws<Claims> claimsJws;
        ObjectRestResponse<SysUser> response = new ObjectRestResponse<>();
        try {
            claimsJws = JWTHelper.parserToken(token, "jwt/pub.key");
            Claims body = claimsJws.getBody();
            String name=body.getSubject();
            ///String id=body.get("id").toString();
            SysUser user = biz.loadByUserName(name);

            List<SysMenu> menuList = new ArrayList<>();
            for (int i = 0; i <user.getRoleList().size() ; i++) {
                menuList.addAll(user.getRoleList().get(i).getMenuList());
            }

            List<TreeNode> treeNodeList = new ArrayList<>();

            getRootNodes(menuList, treeNodeList);

            user.setTreeNodeList(treeNodeList);

            user.setPassword("");
            response.setMessage("获取用户详细信息成功!");
            response.setData(user);
        } catch (Exception e) {
            response.setMessage("获取用户信息失败:"+e.getMessage());
            response.setStatus(500);
            e.printStackTrace();
        }

        return response;
    }

    @GetMapping("/loadByUserName")
    public ObjectRestResponse<SysUser> loadByUserName(String name){
        SysUser user = biz.loadByUserName(name);

        List<SysMenu> menuList = new ArrayList<>();
        for (int i = 0; i <user.getRoleList().size() ; i++) {
            menuList.addAll(user.getRoleList().get(i).getMenuList());
        }
        List<TreeNode> treeNodeList = new ArrayList<>();

        getRootNodes(menuList, treeNodeList);

        user.setTreeNodeList(treeNodeList);
        user.setPassword("");

        return new ObjectRestResponse.Builder<SysUser>().data(user).message("获取用户详细信息成功!").build();
    }

    @GetMapping("/loadResourceByUserName")
    public ObjectRestResponse<SysUser> loadResourceByUserName(String name){
        SysUser user = biz.loadByUserName(name);

        List<SysMenu> menuList = new ArrayList<>();
        for (int i = 0; i <user.getRoleList().size() ; i++) {
            menuList.addAll(user.getRoleList().get(i).getMenuList());
        }
        user.setMenuList(menuList);
        return new ObjectRestResponse.Builder<SysUser>().data(user).message("获取用户详细信息成功!").build();
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("/grantRole")
    public ObjectRestResponse<SysUser> grantRole(@RequestBody SysUser sysUser){
        /// 先清掉所有的角色权限,in操作试用于mysql/oracle
        sysUserRoleService.remove(new QueryWrapper<SysUserRole>()
                                    .in("user_id",sysUser.getId()));

        /// 生成SysUerRole list集合
        String[]roleId = sysUser.getRoleIds().split(",");
        List<SysUserRole> list = new ArrayList<>();
        for (String aRoleId : roleId) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(UUID.randomUUID().toString());
            sysUserRole.setUserId(sysUser.getId());
            sysUserRole.setRoleId(aRoleId);
            list.add(sysUserRole);
        }
        /// 再重写绑定
        sysUserRoleService.saveBatch(list, list.size());

        return new ObjectRestResponse.Builder<SysUser>()
                .message("授权成功!")
                .rel(true)
                .status(200)
                .build();
    }

    @GetMapping("/loadAllRolesTreeNode")
    public ObjectRestResponse<SysUser> loadRolesTreeNode(){
        /// 1.获取所有的角色
        List<SysRole> list = sysRoleService.list(null);
        List<TreeNode> treeNodelist = new ArrayList<>();
        SysUser sysUser = new SysUser();
        /// 2.当list不为空时,遍历
        if (list.size() > 0) {
            list.stream()
                    /// 获取所有的角色根节点
                    .filter((role)-> StringUtils.isBlank(role.getPid()))
                    /// 遍历所有根节点的子节点
                    .forEach(sysRole -> {
                        /// 进行简单的赋值
                        TreeNode treeNode = setTreeNode(sysRole);
                        getTree(list, sysRole, treeNode);
                        treeNodelist.add(treeNode);
                    });
            sysUser.setTreeNodeList(treeNodelist);
            return new ObjectRestResponse.Builder<SysUser>().message("获取所有的角色树成功!").status(200).data(sysUser).build();
        }else{
            return new ObjectRestResponse.Builder<SysUser>().message("数据库为空!").status(200).data(sysUser.setTreeNodeList(null)).build();
        }
    }

    private void getTree(List<SysRole> list, SysRole sysRole, TreeNode treeNode) {
        list.stream().forEach(sysRole2 ->{
            // 下面条件成立,有子节点
            if (sysRole.getId().equals(sysRole2.getPid())) {
                TreeNode node = setTreeNode(sysRole2);
                list.stream().forEach((role)->{
                    if (sysRole.getId().equals(role.getPid())) {
                        getTree(list,role,node);
                    }
                });
                treeNode.getChildren().add(node);
            }
        } );
    }

    private TreeNode setTreeNode(SysRole sysRole) {
        TreeNode treeNode = new TreeNode();
        treeNode.setId(sysRole.getId());
        treeNode.setLabel(sysRole.getName());
        treeNode.setKey(sysRole.getId());
        return treeNode;
    }

    @GetMapping("/loadRolesByUserId")
    public ObjectRestResponse<SysUser> loadRolesByUserId(String id){
        /// 1.通过中间表user-role这张表获取角色id
        List<SysUserRole> list = sysUserRoleService.list(
                new QueryWrapper<SysUserRole>()
                    .eq("user_id",id));

        List<String> roleIds = new ArrayList<>();
        SysUser sysUser = new SysUser();
        /// 2.如果list不为空
        if (list.size() > 0) {
            for (SysUserRole aList : list) {
                roleIds.add(aList.getRoleId());
            }
            /// 获取角色
            List<SysRole> roleList = (List<SysRole>) sysRoleService.listByIds(roleIds);
            List<TreeNode> nodeList = new ArrayList<>();
            roleList.forEach((role)-> nodeList.add(setTreeNode(role)));
            sysUser.setRoleList(roleList);
            sysUser.setTreeNodeList(nodeList);
            sysUser.setId(id);

            return new ObjectRestResponse.Builder<SysUser>()
                    .data(sysUser)
                    .message("获取数据成功")
                    .status(200)
                    .rel(true)
                    .build();
        }else{
            return new ObjectRestResponse.Builder<SysUser>()
                    .data(sysUser.setRoleList(null))
                    .message("获取数据成功")
                    .status(200)
                    .rel(true)
                    .build();
        }

    }


    private void getRootNodes(List<SysMenu> menuList, List<TreeNode> treeNodeList) {
        for (int i = 0; i <menuList.size() ; i++) {
            SysMenu sysMenu = menuList.get(i);
            String pid = sysMenu.getPid();
            if (pid == null || "".equals(pid)){
                TreeNode treeNode = new TreeNode();
                treeNode.setId(sysMenu.getId());
                treeNode.setKey(sysMenu.getId());
                treeNode.setLabel(sysMenu.getName());
                treeNode.setType(sysMenu.getType());
                treeNode.setIcon("");
                treeNode = getTree(treeNode,menuList);
                /// 添加
                treeNodeList.add(treeNode);
            }
        }
    }

    private TreeNode getTree(TreeNode treeNode, List<SysMenu> menuList) {
        for (int i = 0; i <menuList.size() ; i++) {
            SysMenu sysMenu = menuList.get(i);

            if (sysMenu.getPid()!=null&& sysMenu.getPid().equals(treeNode.getId())) {
                TreeNode node = new TreeNode();
                node.setId(sysMenu.getId());
                node.setKey(sysMenu.getId());
                node.setLabel(sysMenu.getName());
                /// 向children 添加
                treeNode.getChildren().add(node);
                /// 递归
                getTree(node, menuList);
            }
        }
        return treeNode;
    }

}

