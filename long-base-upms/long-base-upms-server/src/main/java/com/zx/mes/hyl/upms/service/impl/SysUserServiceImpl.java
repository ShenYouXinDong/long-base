package com.zx.mes.hyl.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.mes.hyl.upms.entity.SysUser;
import com.zx.mes.hyl.upms.mapper.SysUserMapper;
import com.zx.mes.hyl.upms.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Override
    public SysUser loadByUserName(String name) {
        return baseMapper.loadByUserName(name);
    }
}
