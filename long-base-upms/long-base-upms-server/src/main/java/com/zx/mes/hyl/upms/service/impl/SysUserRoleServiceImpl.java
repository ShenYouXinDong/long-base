package com.zx.mes.hyl.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.mes.hyl.upms.entity.SysUserRole;
import com.zx.mes.hyl.upms.mapper.SysUserRoleMapper;
import com.zx.mes.hyl.upms.service.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
