package com.zx.mes.hyl.upms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Jwt implements Serializable {

    private String id;
    private String name;
    private String token;
}
