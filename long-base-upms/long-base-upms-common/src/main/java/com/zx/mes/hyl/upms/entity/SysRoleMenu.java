package com.zx.mes.hyl.upms.entity;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class SysRoleMenu extends Model<SysRoleMenu> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String roleId;
    private String menuId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
