package com.zx.mes.hyl.upms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.zx.mes.hyl.upms.pagemodel.TreeNode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 实现 spring security UserDetails 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class SysUser extends Model<SysUser> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    @TableField("createDateTime")
    private Date createDateTime;
    @TableField("modifyDateTime")
    private Date modifyDateTime;
    private String password;

    @TableField(exist =false)
    private String roleIds;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(exist=false)
    private Date createDateTimeStart;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(exist=false)
    private Date createDatetimeEnd;

    @TableField(exist = false)
    private List<SysRole> roleList = new ArrayList<>();

    @TableField(exist = false)
    private List<TreeNode> treeNodeList = new ArrayList<>();

    @TableField(exist = false)
    private List<SysMenu> menuList = new ArrayList<>();

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
