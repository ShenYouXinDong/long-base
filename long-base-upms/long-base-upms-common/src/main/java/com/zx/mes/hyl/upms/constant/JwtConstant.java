package com.zx.mes.hyl.upms.constant;

/**
 * jwt constant
 * @author 华云龙
 * @date 2018-10-10
 * @since 1.0
 */
public interface JwtConstant {

    /**
     * jwt 中存储的id
     */
    String JWT_KEY_USER_ID = "id";

    /**
     * jwt 中存储的name
     */
    String JWT_KEY_USER_NAME = "name";
}
