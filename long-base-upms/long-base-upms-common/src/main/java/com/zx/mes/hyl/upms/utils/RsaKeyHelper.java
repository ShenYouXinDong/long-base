package com.zx.mes.hyl.upms.utils;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 读取公私钥,并解析
 * @author hyl
 * @date 2018-6-29
 */
public class RsaKeyHelper {
    /**
     * 获取公钥(根据文件路径读取公钥,然后解析出公钥)
     * @param filename
     * @return PublicKey
     * @throws Exception
     */
    public PublicKey getPublicKey(String filename) throws Exception {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(filename);
        DataInputStream dis = new DataInputStream(resourceAsStream);
        byte[] keyBytes = new byte[resourceAsStream.available()];
        dis.readFully(keyBytes);
        dis.close();
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    /**
     * 获取私钥
     * @param filename
     * @return PrivateKey
     * @throws Exception
     */
    public PrivateKey getPrivateKey(String filename) throws Exception {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(filename);
        DataInputStream dis = new DataInputStream(resourceAsStream);
        // 通过私钥byte[]将公钥还原，适用于RSA算法
        byte[] keyBytes = new byte[resourceAsStream.available()];
        dis.readFully(keyBytes);
        dis.close();
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    /**
     * 生存rsa公钥和密钥
     * @param publicKeyFilename
     * @param privateKeyFilename
     * @param password
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static void generateKey(String publicKeyFilename, String privateKeyFilename, String password) throws IOException, NoSuchAlgorithmException {
        // RSA的公钥和私钥是由KeyPairGenerator生成
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        // 加入密码 盐
        SecureRandom secureRandom = new SecureRandom(password.getBytes());
        // 密钥位数 设置密钥位数越高，加密过程越安全，一般使用1024位
        keyPairGenerator.initialize(1024, secureRandom);
        // 动态生成密钥对
        KeyPair keyPair = keyPairGenerator.genKeyPair();
        // 公钥
        byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
        FileOutputStream fos = new FileOutputStream(publicKeyFilename);
        fos.write(publicKeyBytes);
        fos.close();
        // 私钥
        byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();
        fos = new FileOutputStream(privateKeyFilename);
        fos.write(privateKeyBytes);
        fos.close();
    }

    public static void test() throws IOException, NoSuchAlgorithmException {
        RsaKeyHelper.generateKey("C:\\Users\\Administrator\\Desktop\\test\\pub.key","C:\\Users\\Administrator\\Desktop\\test\\pri.key","*632105841@qq.com?");
    }

    public static void main(String[]args){
        try {
            test();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

}


