package com.zx.mes.hyl.upms.pagemodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class VueRouterNode {

    private String path;

    private String name;

    private String iconCls;

    private String component;

    private VueRouterNode node;

}
