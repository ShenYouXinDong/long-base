package com.zx.mes.hyl.upms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-07-09
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class SysRole extends Model<SysRole> {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String remark;
    private String pid;
    private Integer level;
    @TableField(exist = false)
    private String menuIds;

    @TableField(exist = false)
    private List<SysMenu> menuList = new ArrayList<>();

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
