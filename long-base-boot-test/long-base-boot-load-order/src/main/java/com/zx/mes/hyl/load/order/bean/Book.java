package com.zx.mes.hyl.load.order.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * book bean
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class Book {
    private String name;
    private Integer age;
}
