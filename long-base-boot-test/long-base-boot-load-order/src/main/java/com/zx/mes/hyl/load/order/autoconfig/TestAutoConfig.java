package com.zx.mes.hyl.load.order.autoconfig;

import com.zx.mes.hyl.load.order.bean.Book;
import com.zx.mes.hyl.load.order.imports.MyImportBeanDefinitionRegistrar;
import com.zx.mes.hyl.load.order.imports.MyImportSelector;
import com.zx.mes.hyl.load.order.properties.PersonProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * auto config bean
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@Configuration
@EnableConfigurationProperties(PersonProperties.class)
@Import({MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})
public class TestAutoConfig {

    @Bean
    public Book book(){
        return new Book("书名cc", 11);
    }

    @Bean
    public Book book2(){
        return new Book("书名cc2", 22);
    }
}
