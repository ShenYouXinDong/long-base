package com.zx.mes.hyl.load.order.bean;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * cat bean
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@Data
@ToString
@Accessors(chain = true)
public class Cat {
    private String name;
    private Integer age;
}
