package com.zx.mes.hyl.load.order.bean;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * dog bean
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@Data
@Accessors(chain = true)
@ToString
public class Dog {
    private String name;
    private Integer age;
}
