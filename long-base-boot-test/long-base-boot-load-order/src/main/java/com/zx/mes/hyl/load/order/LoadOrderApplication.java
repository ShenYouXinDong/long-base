package com.zx.mes.hyl.load.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot bean load order
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@SpringBootApplication
public class LoadOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(LoadOrderApplication.class, args);
    }
}
