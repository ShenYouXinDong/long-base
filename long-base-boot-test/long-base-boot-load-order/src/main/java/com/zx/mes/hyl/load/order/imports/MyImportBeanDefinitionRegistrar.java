package com.zx.mes.hyl.load.order.imports;

import com.zx.mes.hyl.load.order.bean.Cat;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * org.springframework.context.annotation.ImportBeanDefinitionRegistrar
 *
 * @author 华云龙
 * @date 2018-11-20
 */
public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        BeanDefinition beanDefinition = new RootBeanDefinition(Cat.class);
        registry.registerBeanDefinition(Cat.class.getName(), beanDefinition);
    }
}
