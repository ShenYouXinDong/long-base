package com.zx.mes.hyl.load.order.imports;

import com.zx.mes.hyl.load.order.bean.Dog;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * org.springframework.context.annotation.ImportSelector 实现
 *
 * @author 华云龙
 * @date 2018-11-20
 */
public class MyImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{Dog.class.getName()};
    }
}
