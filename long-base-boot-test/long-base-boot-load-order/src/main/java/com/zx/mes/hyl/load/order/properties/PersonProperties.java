package com.zx.mes.hyl.load.order.properties;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * properties config
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@Data
@ToString
@Accessors(chain = true)
@ConfigurationProperties(prefix = "person")
public class PersonProperties{
    private String name;
    private Integer age;
}
