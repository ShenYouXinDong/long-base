package com.zx.mes.hyl.load.order;

import com.zx.mes.hyl.load.order.bean.Book;
import com.zx.mes.hyl.load.order.properties.PersonProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * load order test
 *
 * @author 华云龙
 * @date 2018-11-20
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class LoadOrderApplicationTest implements BeanFactoryAware {

    private ConfigurableListableBeanFactory beanFactory;

    @Autowired
    private PersonProperties personProperties;

    @Test
    public void test() {
        /// 获取指定类型所有的bean实例
        String[] beanNamesForType = beanFactory.getBeanNamesForType(Book.class);
        Arrays.stream(beanNamesForType).forEach(System.out::println);

        System.out.println("----------------------------------------------");
        /// 打印所有beanName
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        Stream.of(beanDefinitionNames).forEach(System.out::println);
    }

    /**
     * properties
     */
    @Test
    public void test2(){
        System.out.println(personProperties.toString());
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        if (beanFactory instanceof ConfigurableListableBeanFactory) {
            this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
        }
    }
}
