package com.zx.mes.hyl;

import com.zx.mes.hyl.properties.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * properties config test
 *
 * @author 华云龙
 * @date 2018-11-8
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BootStrapTest {

    @Autowired
    private Person person;

    @Test
    public void test(){
        System.out.println(person.toString());
    }
}
