package com.zx.mes.hyl.properties;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * yaml config
 *
 * @author 华云龙
 * @date 2018-11-6
 */
@Component
@ConfigurationProperties(prefix = "person")
@Data
@ToString
@Accessors(chain = true)
public class Person {

    private String name;
    private Integer age;
    private String id;
    private Date date;
    private Map<String, Object> map;
    private List<String> list;
    private Dog dog;
}
