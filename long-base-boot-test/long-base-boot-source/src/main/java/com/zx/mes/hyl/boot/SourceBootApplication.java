package com.zx.mes.hyl.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot 源码测试
 * @author 华云龙
 * @date 2018-10-16
 */
@SpringBootApplication
public class SourceBootApplication {
    public static void main(String[] args) {
        Class clzz = SourceBootApplication.class;
        /// com.zx.mes.hyl.boot.SourceBootApplication
        System.out.println(clzz.getName());
        SpringApplication.run(SourceBootApplication.class, args);
    }
}
