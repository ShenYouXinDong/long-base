package com.zx.mes.hyl.yaml;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * yaml test
 * @SpringBootTest(classes = BootStrap.class)  填入 真正的引导类  ,第二天再测已无问题,官网写法又是正确(??????)
 *
 * @author 华云龙
 * @date 2018-11-7
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class YamlTest implements BeanFactoryAware{

    private  ListableBeanFactory beanFactory;

    @Autowired
    private Person person;

    @Test
    public void test(){
        System.out.println(person.toString());
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = (ListableBeanFactory) beanFactory;
    }
}
