package com.zx.mes.hyl.controller;

import com.zx.mes.hyl.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试 springMvc params bind
 *
 * @author 华云龙
 * @date 2018-11-22
 */
@RestController
public class UserController {

    @GetMapping("/user")
    public User getUser(User user) {
        return new User(user.getName(), user.getAge(), user.getDate());
    }
}
