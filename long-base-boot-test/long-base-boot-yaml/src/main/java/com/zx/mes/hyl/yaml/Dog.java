package com.zx.mes.hyl.yaml;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * properties config
 *
 * @author 华云龙
 * @date 2018-11-7
 */
@Data
@Accessors(chain = true)
@ToString
public class Dog {
    private String name;
}
