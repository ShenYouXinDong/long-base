package com.zx.mes.hyl.yaml;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * yaml config
 * 1.自动提示
 * 2.对所配置的值进行校验
 * 3.常见配置值的写法
 *
 * @author 华云龙
 * @date 2018-11-6
 */
@Component
@Validated
@ConfigurationProperties(prefix = "person")
@Data
@ToString
@Accessors(chain = true)
public class Person {

    @NotNull
    private String name;
    private Integer age;
    private String id;
    private Date date;
    private Map<String, Object> map;
    private List<String> list;
    private Dog dog;
    private Cat cat;

    /**
     * 写成内部类的行式,可以解决yaml不提示问题
     */
    @Data
    @Accessors(chain = true)
    public static class Cat{
        private String name;
        private Integer age;
    }
}
