package com.zx.mes.hyl.config;

import org.springframework.core.convert.converter.Converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * date convert
 *  例如:用js生成当前的时间作为参数
 * @author 华云龙
 * @date 2018-11-22
 */
public class DateConverterConfig implements Converter<String, Date> {

    private static final List<String> DATE_FORMATTER = new ArrayList<>(4);


    static {
        DATE_FORMATTER.add("yyyy-MM");
        DATE_FORMATTER.add("yyyy-MM-dd");
        DATE_FORMATTER.add("yyyy-MM-dd hh:mm");
        DATE_FORMATTER.add("yyyy-MM-dd hh:mm:ss");
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Date convert(String source) {
        String value = source.trim();
        if ("".equals(value)) {
            return null;
        }
        if (source.matches("^\\d{4}-\\d{1,2}$")) {
            return parseDate(source, DATE_FORMATTER.get(0));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
            return parseDate(source, DATE_FORMATTER.get(1));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
            return parseDate(source, DATE_FORMATTER.get(2));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
            return parseDate(source, DATE_FORMATTER.get(3));
        } else {
            throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
        }
    }

    /**
     * 格式化日期
     *
     * @param dateStr String 字符型日期
     * @param format  String 格式
     * @return Date 日期
     */
    private Date parseDate(String dateStr, String format) {
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            date = dateFormat.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

}


