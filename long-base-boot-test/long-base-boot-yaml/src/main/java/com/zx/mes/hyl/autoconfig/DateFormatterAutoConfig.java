package com.zx.mes.hyl.autoconfig;

import com.zx.mes.hyl.config.DateConverterConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

/**
 * date formatter
 *
 * @author 华云龙
 * @date 2018-11-22
 */
@Configuration
public class DateFormatterAutoConfig {

    @Bean
    public Converter<String,Date> dateConverter(){
        return new DateConverterConfig();
    }
}
