package com.zx.mes.hyl.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 解决时间绑定问题
 *
 * @author 华云龙
 * @date 2018-11-22
 */
@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class User {

    private String name;

    private Integer age;

    private Date date;
}
