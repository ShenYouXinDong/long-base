package com.zx.mes.poi.hyl.service.impl;

import com.zx.mes.poi.hyl.entity.Pcc1w7;
import com.zx.mes.poi.hyl.mapper.Pcc1w7Mapper;
import com.zx.mes.poi.hyl.service.Pcc1w7Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2019-01-11
 */
@Service
public class Pcc1w7ServiceImpl extends ServiceImpl<Pcc1w7Mapper, Pcc1w7> implements Pcc1w7Service {

}
