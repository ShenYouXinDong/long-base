package com.zx.mes.poi.hyl.controller;

import cn.afterturn.easypoi.cache.manager.POICacheManager;
import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.ExcelToHtmlParams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * excel to html controller test
 *
 * @author 华云龙
 * @date 2019-1-11
 */
@Slf4j
@RestController
@RequestMapping("/excelToHtml")
public class ExcelToHtmlController {

    private static final Pattern R = Pattern.compile("\\)\\w+</td>");
    private static final int INITIAL_CAPACITY = 128;

    /**
     * 07 版本EXCEL预览
     */
    @SuppressWarnings("AlibabaAvoidPatternCompileInMethod")
    @RequestMapping("07")
    public void toHtmlOf07Base(HttpServletResponse response) throws IOException, InvalidFormatException {
//        ExcelToHtmlParams params = new ExcelToHtmlParams(WorkbookFactory.create(POICacheManager.getFile("C:\\Users\\Administrator\\Desktop\\BL.XLS")));
//        String html = ExcelXorHtmlUtil.excelToHtml(params);
//        Matcher m = R.matcher(html);
//
//        List<String> list = new ArrayList<>(INITIAL_CAPACITY);
//        while (m.find()) {
//            list.add(m.group().substring(1, m.group().length() - 5));
//        }
//
//        for (String str : list) {
//            html = html.replaceFirst("\\)" + str, "{{data." + str.substring(0, str.length()) + "}}");
//        }
//        response.getOutputStream().write(html.getBytes());

    }
}
