package com.zx.mes.poi.hyl.service;

import com.zx.mes.poi.hyl.entity.Pcc1w7;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2019-01-11
 */
public interface Pcc1w7Service extends IService<Pcc1w7> {

}
