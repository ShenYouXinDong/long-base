package com.zx.mes.poi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * poi 由excel 生成 html
 *
 * @author 华云龙
 * @date 2019-1-11
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.zx.mes.poi.hyl.mapper")
public class PoiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PoiApplication.class);
    }
}
