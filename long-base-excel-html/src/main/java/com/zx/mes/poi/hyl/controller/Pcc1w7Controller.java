package com.zx.mes.poi.hyl.controller;

import com.zx.mes.hyl.controller.BaseController;
import com.zx.mes.hyl.response.ObjectRestResponse;
import com.zx.mes.poi.hyl.mapper.Pcc1w7Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zx.mes.poi.hyl.entity.Pcc1w7;
import com.zx.mes.poi.hyl.service.Pcc1w7Service;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2019-01-11
 */
@RestController
@RequestMapping("/pcc1w7")
public class Pcc1w7Controller extends BaseController<Pcc1w7Service, Pcc1w7> {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private Pcc1w7Mapper pcc1w7Mapper;

    @GetMapping("/getBL")
    public ObjectRestResponse<Pcc1w7> getBL(String w7key1, String w7key7, String w7key5) {
        ObjectRestResponse.Builder<Pcc1w7> response = new ObjectRestResponse.Builder<>();
        Pcc1w7 pcc1w7 = pcc1w7Mapper.getBL(w7key1, w7key7, w7key5);
        response.data(pcc1w7).message("成功获取BL的报表信息!");
        return response.build();
    }

}

