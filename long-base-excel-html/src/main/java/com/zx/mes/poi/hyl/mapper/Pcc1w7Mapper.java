package com.zx.mes.poi.hyl.mapper;

import com.zx.mes.poi.hyl.entity.Pcc1w7;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2019-01-11
 */
public interface Pcc1w7Mapper extends BaseMapper<Pcc1w7> {
    /**
     * 加流 BL.cel
     *
     * @param w7key1 w7key1
     * @param w7key7 w7key7
     * @param w7key5 w7key5
     * @return Pcc1w7
     */
    Pcc1w7 getBL(@Param("w7key1") String w7key1,@Param("w7key7") String w7key7,@Param("w7key5") String w7key5);
}
