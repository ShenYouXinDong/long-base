package com.zx.mes.poi.hyl.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2019-01-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Pcc1w7 extends Model<Pcc1w7> {

    private static final long serialVersionUID = 1L;

    private String w7key1;

    private String w7key2;

    private String w7key3;

    private String w7key4;

    private String w7key5;

    private String w7key6;

    private String w7key7;

    private String w7key8;

    private String d1;

    private String b1;

    private String a64a;

    private String a45a;

    private String a5a;

    private String a11a;

    private String a98a;

    private String a1;

    private String a2;

    private String a3;

    private String a4;

    private String a5;

    private String a6;

    private String a7;

    private String a8;

    private String a9;

    private String a10;

    private String a11;

    private String a12;

    private String a13;

    private String a14;

    private String a15;

    private String a16;

    private String a17;

    private String a18;

    private String a19;

    private String a20;

    private String a21;

    private String a22;

    private String a23;

    private String a24;

    private String a25;

    private String a26;

    private String a27;

    private String a28;

    private String a29;

    private String a30;

    private String a31;

    private String a32;

    private String a33;

    private String a34;

    private String a35;

    private String a36;

    private String a37;

    private String a38;

    private String a39;

    private String a40;

    private String a41;

    private String a42;

    private String a43;

    private String a44;

    private String a45;

    private String a46;

    private String a47;

    private String a48;

    private String a49;

    private String a50;

    private String a51;

    private String a52;

    private String a53;

    private String a54;

    private String a55;

    private String a56;

    private String a57;

    private String a58;

    private String a59;

    private String a60;

    private String a61;

    private String a62;

    private String a63;

    private String a64;

    private String a65;

    private String a66;

    private String a67;

    private String a68;

    private String a69;

    private String a70;

    private String a71;

    private String a72;

    private String a73;

    private String a74;

    private String a75;

    private String a76;

    private String a77;

    private String a78;

    private String a79;

    private String a80;

    private String a81;

    private String a82;

    private String a83;

    private String a84;

    private String a85;

    private String a86;

    private String a87;

    private String a88;

    private String a89;

    private String a90;

    private String a91;

    private String a92;

    private String a93;

    private String a94;

    private String a95;

    private String a96;

    private String a97;

    private String a98;

    private String a99;

    private String a100;

    private String a101;

    private String a102;

    private String a103;

    private String a104;

    private String a105;

    private String a106;

    private String a107;

    private String a108;

    private String a109;

    private String a110;

    private String a111;

    private String a112;

    private String a113;

    private String a114;

    private String a115;

    private String a116;

    private String a117;

    private String a118;

    private String a119;

    private String a120;

    private String a121;

    private String a122;

    private String a123;

    private String a124;

    private String a125;

    private String a126;

    private String a127;

    private String a128;

    private String a129;

    private String a130;

    private String a131;

    private String a132;

    private String a133;

    private String a134;

    private String a135;

    private String a136;

    private String a137;

    private String a138;

    private String a139;

    private String a140;

    private String a141;

    private String a142;

    private String a143;

    private String a144;

    private String a145;

    private String a146;

    private String a147;

    private String a148;

    private String a149;

    private String a150;

    private String a151;

    private String a152;

    private String a153;

    private String a154;

    private String a155;

    private String a156;

    private String a157;

    private String a158;

    private String a159;

    private String a160;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
