package com.zx.mes.poi.hyl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Pattern test
 * https://www.cnblogs.com/yw0219/p/8047938.html
 *
 * @author 华云龙
 * @date 2019-1-11
 */
public class PatternTest {

    public static void main(String[] args) {
        String line = "<td class='style_11b_985 font_252_985'>)a84_1</td><td class='style_11c_985 font_253_985'>)a84_2</td>\n" +
                "        <td class='style_11d_985 font_254_985'>)a84_3</td>\n" +
                "        <td class='style_11e_985 font_255_985'>)a84_4</td>\n" +
                "        <td class='style_11f_985 font_256_985'>)a84_5</td>\n" +
                "        <td class='style_120_985 font_257_985'>)a84_6</td>\n" +
                "        <td class='style_121_985 font_258_985'>)a84_7</td>\n" +
                "        <td class='style_122_985 font_259_985'>)a84_8</td>";
            String pattern = "\\)\\w+</td>";
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);

        List<String> list = new ArrayList<>(128);
        while (m.find()) {
            System.out.println("整个匹配结果=>" + m.group());
//            int length = m.group().length();
            list.add(m.group().substring(1, m.group().length() - 5));
//            System.out.println("{{data." +m.group().substring(1, length - 5)+ "}}");
//            m.replaceFirst("{{data." + m.group().substring(1, length - 5) + "}}");
        }

        System.out.println(line);
        for (int i = 0; i < list.size(); i++) {
            line = line.replaceFirst("\\)" + list.get(i), "{{data." + list.get(i).substring(1, list.get(i).length()) + "}}");
        }
        System.out.println(line);

    }

    public void test() {
        // 按指定模式在字符串查找
        String line = "This order was placed for QT3000! OK?";
        String pattern = "(\\D*)(\\d+)(.*)";

        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);

        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        if (m.find()) {
            System.out.println("Found value: " + m.group(0));
            System.out.println("Found value: " + m.group(1));
            System.out.println("Found value: " + m.group(2));
            System.out.println("Found value: " + m.group(3));
        } else {
            System.out.println("NO MATCH");
        }
    }
}
