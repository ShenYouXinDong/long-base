package com.zx.mes.hyl.creator;

import com.zx.mes.hyl.properties.CreatorProperties;
import com.zx.mes.hyl.properties.DataSourceType;
import com.zx.mes.hyl.service.mysql.impl.ColumnsServiceImpl;
import com.zx.mes.hyl.service.mysql.impl.TablesServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 根据数据库类型的不同,导入不同的类
 *
 * @author 华云龙
 * @date 2018-11-11
 */
public class CreatorImportSelector implements ImportSelector{

    @Autowired
    private CreatorProperties creatorProperties;

    private static final String[] IMPORTS_MYSQL = {
            ColumnsServiceImpl.class.getName(),
            TablesServiceImpl.class.getName(),
            MapperScanConfiguration.class.getName()
    };

    @SuppressWarnings("NullableProblems")
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        if (StringUtils.equalsIgnoreCase(creatorProperties.getDbType(), DataSourceType.MYSQL)) {
            return IMPORTS_MYSQL;
        }
        return new String[0];
    }
}
