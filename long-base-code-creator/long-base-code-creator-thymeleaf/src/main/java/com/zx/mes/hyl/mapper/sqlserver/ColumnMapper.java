package com.zx.mes.hyl.mapper.sqlserver;

import com.zx.mes.hyl.entity.sqlserver.Columns;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * sqlServer column mapper
 *
 * @author 华云龙
 * @date 2018-11-21
 */
public interface ColumnMapper {

    /**
     * 根据表名获取表中的列名与列名类型
     *
     * @param tableName 表名
     * @return Columns
     */
    @Select("select c.name as columnName,c.xtype as columnType ,t.name columnTypeName from syscolumns c " +
            "left join systypes t " +
            "on c.xtype=t.xtype " +
            "where  c.id=OBJECT_ID(#{tableName})")
    List<Columns> getColumnsByTableName(String tableName);
}
