package com.zx.mes.hyl.autoconfig;

import com.zx.mes.hyl.entity.Book;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * SqlServer code creator auto config
 *
 * @author 华云龙
 * @date 2018-11-21
 */
@Configuration
@ConditionalOnProperty(prefix = "hyl.creator",name = "dbType",havingValue = "sqlServer")
@MapperScan("com.zx.mes.hyl.mapper.sqlserver")
public class SqlServerCreatorAutoConfig {

    @Bean
    public Book book(){
        return new Book("书本", 11);
    }
}
