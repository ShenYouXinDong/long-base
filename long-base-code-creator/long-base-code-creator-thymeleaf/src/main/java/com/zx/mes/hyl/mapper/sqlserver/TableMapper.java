package com.zx.mes.hyl.mapper.sqlserver;

import com.zx.mes.hyl.entity.sqlserver.Table;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * table mapper
 *
 * @author 华云龙
 * @date 2018-11-21
 */
public interface TableMapper {

    /**
     * 获取指定数据库表
     *
     * @return Table
     */
    @Select(" SELECT name as tableName FROM SysObjects Where XType='U' ORDER BY Name")
    List<Table> getTables();
}
