package com.zx.mes.hyl.service.mysql.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.mes.hyl.entity.mysql.Columns;
import com.zx.mes.hyl.mapper.mysql.ColumnsMapper;
import com.zx.mes.hyl.service.mysql.ColumnsService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-11-11
 */
public class ColumnsServiceImpl extends ServiceImpl<ColumnsMapper, Columns> implements ColumnsService {

}
