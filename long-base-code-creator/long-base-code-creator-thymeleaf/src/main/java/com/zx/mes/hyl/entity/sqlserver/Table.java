package com.zx.mes.hyl.entity.sqlserver;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * sqlServer table entity
 *
 * @author 华云龙
 * @date 2018-11-21
 */
@Data
@Accessors(chain = true)
@ToString
public class Table {

    /**
     * 表的名称
     */
    private String tableName;
}
