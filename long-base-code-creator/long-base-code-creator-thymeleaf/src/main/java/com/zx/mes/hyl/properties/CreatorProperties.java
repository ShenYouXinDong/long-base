package com.zx.mes.hyl.properties;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * 绑定配置 信息
 *
 * @author 华云龙
 * @date 2018-11-11
 */
@Data
@Accessors(chain = true)
@ToString
@ConfigurationProperties(prefix = "hyl.creator")
public class CreatorProperties {

    /**
     * 数据库类型
     */
    private String dbType=DataSourceType.MYSQL;

    /**
     * dataBase
     */
    private String db;

    /**
     * code dir
     */
    private String codeDir;

    /**
     * spring application name
     */
    private String applicationName;

    /**
     * 需要生成的表(配置)
     */
    private List<String> includes = new ArrayList<>(8);

    /**
     * 一库下面所有表,刨除不要生成的表(配置)
     */
    private List<String> excludes = new ArrayList<>(8);

    /**
     * mysql config
     */
    private Mysql mysql = new Mysql();

    /**
     * sqlServer config
     */
    private SqlServer sqlServer = new SqlServer();

    /**
     * oracle config
     */
    private Oracle oracle = new Oracle();

    @Data
    @Accessors(chain = true)
    @ToString
    public  class Mysql{
        /**
         * dataBase
         */
        private String db;
    }

    @Data
    @Accessors(chain = true)
    @ToString
    public class SqlServer{

    }

    @Data
    @Accessors(chain = true)
    @ToString
    public class Oracle{

    }
}
