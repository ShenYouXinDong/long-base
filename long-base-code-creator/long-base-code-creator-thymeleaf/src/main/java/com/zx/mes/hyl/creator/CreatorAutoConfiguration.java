package com.zx.mes.hyl.creator;

import com.zx.mes.hyl.autoconfig.MySqlCreatorAutoConfig;
import com.zx.mes.hyl.autoconfig.SqlServerCreatorAutoConfig;
import com.zx.mes.hyl.properties.CreatorProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.sql.DataSource;

/**
 * 代码生成器 自动装配
 *
 * @author 华云龙
 * @date 2018-11-11
 */
@Configuration
@EnableConfigurationProperties(CreatorProperties.class)
@ConditionalOnClass(value = {DataSource.class})
@Import({MySqlCreatorAutoConfig.class, SqlServerCreatorAutoConfig.class})
public class CreatorAutoConfiguration {
//
//    /// -------------------------------------   MYSQL配置 start  -------------------------------------------
//
//    @ConditionalOnProperty(prefix = "hyl.creator",value = "dbType",havingValue = "mysql",matchIfMissing = true)
//    @Bean
//    public ColumnsService columnsService(){
//        return new ColumnsServiceImpl();
//    }
//
//    @ConditionalOnProperty(prefix = "hyl.creator",value = "dbType",havingValue = "mysql",matchIfMissing = true)
//    @Bean
//    public TablesService tablesService(){
//        return new TablesServiceImpl();
//    }
//
//    /// -------------------------------------   MYSQL配置 end  -------------------------------------------
}
