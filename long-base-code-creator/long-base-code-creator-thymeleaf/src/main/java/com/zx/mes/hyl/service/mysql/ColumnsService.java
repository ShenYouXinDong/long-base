package com.zx.mes.hyl.service.mysql;

import com.zx.mes.hyl.entity.mysql.Columns;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-11-11
 */
public interface ColumnsService extends IService<Columns> {

}
