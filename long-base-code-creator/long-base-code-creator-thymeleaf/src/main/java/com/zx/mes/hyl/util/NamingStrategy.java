package com.zx.mes.hyl.util;

import com.zx.mes.hyl.constant.ConstVal;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * 字符串命名策略
 *
 * @author 华云龙
 * @date 2018-11-13
 */
public enum NamingStrategy {

    /**
     * 不做任何改变，原样输出
     */
    nochange,
    /**
     * 下划线转驼峰命名
     */
    underline_to_camel,

    /**
     * 驼峰转下划线命名
     */
    camel_to_underline;


    /**
     * 下划线转驼峰
     * 例如:
     * xx_aa -> xxAa
     * xx__aa ->xxAa
     * _xx_aa -> xxAa
     * xx_aa_ -> xxAa
     *
     * @param str 字符串
     * @return 返回驼峰字符串
     */
    public static String underlineToCamel(String str) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }

        StringBuilder sb = new StringBuilder();

        /// 跳过原始字符串开头与结尾的下划线或双重下划线
        /// 处理真正的驼峰片段
        String[] camels = str.split(ConstVal.UNDERLINE);
        Arrays.stream(camels).filter(camel -> !StringUtils.isEmpty(camel)).forEach(camel -> {
            if (sb.length() == 0) {
                /// 第一个驼峰片段,全部都是小写
                sb.append(camel);
            } else {
                /// 其它的驼峰片段,首字母大写
                sb.append(capitalFirst(camel));
            }
        });
        return sb.toString();
    }

    /**
     * 首字母大写,其它的不变
     *
     * @param name
     * @return 若是空, 则直接返回原来的字符串, 若不空首字母大写
     */
    public static String capitalFirst(String name) {
        if (StringUtils.isEmpty(name)) {
            return name;
        }
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
