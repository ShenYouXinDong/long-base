package com.zx.mes.hyl.util;

import java.io.File;

/**
 * 文件工具类
 *
 * @author 华云龙
 * @date 2018-11-15
 */
public class FileUtils {

    /**
     * 不管是文件夹不存,还是文件不存在,全部创建
     *
     * @param fileName
     */
    public static boolean createFileOrDir(String fileName) {
        boolean mkdirs = false;
        File file = new File(fileName);
        if (!file.exists()) {
            mkdirs = file.mkdirs();
        }
        return mkdirs;
    }
}
