package com.zx.mes.hyl.pagemodel;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 返回需要表的列信息
 *
 * @author 华云龙
 * @date 2018-11-15
 */
@Data
@Accessors(chain = true)
public class Columns {

    private String columnName;

    private String dataType;
}
