package com.zx.mes.hyl.service.mysql.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.mes.hyl.entity.mysql.Tables;
import com.zx.mes.hyl.mapper.mysql.TablesMapper;
import com.zx.mes.hyl.service.mysql.TablesService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-11-11
 */
public class TablesServiceImpl extends ServiceImpl<TablesMapper, Tables> implements TablesService {

}
