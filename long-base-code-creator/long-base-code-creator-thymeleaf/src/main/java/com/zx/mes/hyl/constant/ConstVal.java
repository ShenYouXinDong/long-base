package com.zx.mes.hyl.constant;

/**
 * 常量
 *
 * @author 华云龙
 * @date 2018-11-13
 */
public interface ConstVal {

    /**
     * 下划线
     */
    String UNDERLINE = "_";
}
