package com.zx.mes.hyl.mapper.mysql;

import com.zx.mes.hyl.entity.mysql.Tables;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-11-11
 */
public interface TablesMapper extends BaseMapper<Tables> {

}
