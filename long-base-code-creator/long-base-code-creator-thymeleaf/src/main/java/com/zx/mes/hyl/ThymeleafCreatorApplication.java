package com.zx.mes.hyl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Vue 代码生成器
 *
 * @author 华云龙
 * @date 2018-11-8
 */
@SpringBootApplication(scanBasePackages = "com.zx.mes.hyl")
public class ThymeleafCreatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeleafCreatorApplication.class, args);
    }
}
