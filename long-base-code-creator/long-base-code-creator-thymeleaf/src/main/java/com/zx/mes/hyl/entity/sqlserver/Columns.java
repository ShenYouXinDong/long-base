package com.zx.mes.hyl.entity.sqlserver;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * sqlServer column entity
 *
 * @author 华云龙
 * @date 2018-11-21
 */
@Data
@ToString
@Accessors(chain = true)
public class Columns {
    /**
     * 列名
     */
    private String columnName;

    /**
     * 类型
     */
    private Integer columnType;

    /**
     * 类型名
     */
    private String columnTypeName;
}
