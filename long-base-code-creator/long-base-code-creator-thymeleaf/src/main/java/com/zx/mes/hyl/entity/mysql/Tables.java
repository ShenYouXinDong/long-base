package com.zx.mes.hyl.entity.mysql;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-11-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Tables extends Model<Tables> {

    private static final long serialVersionUID = 1L;

    private String tableCatalog;

    private String tableSchema;

    private String tableName;

    private String tableType;

    private String engine;

    private Long version;

    private String rowFormat;

    private Long tableRows;

    private Long avgRowLength;

    private Long dataLength;

    private Long maxDataLength;

    private Long indexLength;

    private Long dataFree;

    private Long autoIncrement;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private LocalDateTime checkTime;

    private String tableCollation;

    private Long checksum;

    private String createOptions;

    private String tableComment;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
