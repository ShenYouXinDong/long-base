package com.zx.mes.hyl.creator;

import org.mybatis.spring.annotation.MapperScan;

/**
 * mapper scan 导入
 *
 * @author 华云龙
 * @date 2018-11-11
 */
@MapperScan("com.zx.mes.hyl.mapper")
public class MapperScanConfiguration {
}
