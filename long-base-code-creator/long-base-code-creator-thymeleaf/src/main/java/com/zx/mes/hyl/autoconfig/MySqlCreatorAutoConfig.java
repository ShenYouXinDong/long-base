package com.zx.mes.hyl.autoconfig;

import com.zx.mes.hyl.entity.Book;
import com.zx.mes.hyl.service.mysql.impl.ColumnsServiceImpl;
import com.zx.mes.hyl.service.mysql.impl.TablesServiceImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * mysql creator auto config
 *
 * @author 华云龙
 * @date 2018-11-21
 */
@Configuration
@ConditionalOnProperty(prefix = "hyl.creator",name = "dbType",havingValue = "mysql")
@Import({ColumnsServiceImpl.class, TablesServiceImpl.class})
@MapperScan("com.zx.mes.hyl.mapper.mysql")
public class MySqlCreatorAutoConfig {

    @Bean
    public Book book2(){
        return new Book("书本22", 22);
    }
}
