package com.zx.mes.hyl.properties;

/**
 * 数据库种类
 *
 * @author 华云龙
 * @date 2018-11-11
 */
public interface DataSourceType {

    /**
     * mysql
     */
    String MYSQL = "mysql";

    /**
     * oracle
     */
    String ORACLE = "oracle";

    /**
     * sqlserver
     */
    String SQLSERVER = "sqlServer";

}
