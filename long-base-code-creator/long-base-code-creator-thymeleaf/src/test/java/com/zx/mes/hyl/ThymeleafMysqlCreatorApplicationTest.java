package com.zx.mes.hyl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zx.mes.hyl.entity.mysql.Columns;
import com.zx.mes.hyl.entity.mysql.Tables;
import com.zx.mes.hyl.properties.CreatorProperties;
import com.zx.mes.hyl.service.mysql.ColumnsService;
import com.zx.mes.hyl.service.mysql.TablesService;
import com.zx.mes.hyl.service.mysql.impl.TablesServiceImpl;
import com.zx.mes.hyl.util.FileUtils;
import com.zx.mes.hyl.util.NamingStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.context.AbstractContext;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * Thymeleaf creator application test
 *
 * @author 华云龙
 * @date 2018-11-8
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.zx.mes.hyl.mapper")
public class ThymeleafMysqlCreatorApplicationTest implements BeanFactoryAware {

    /**
     * mysql 过滤获取表的类型
     */
    private static final String BASE_TABLE = "BASE TABLE";

    /**
     * 指定生成vue文件的模板名称
     */
    private static final String TEMPLATE = "vue";

    /**
     * 生成vue文件的后缀
     */
    private static final String SUFFIX = ".vue";

    /**
     * 生成Vue文件的名称
     */
    public static final String INDEX = "index";
    @Autowired
    private SpringResourceTemplateResolver defaultTemplateResolver;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private TablesService tablesService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ColumnsService columnsService;

    @Autowired
    private CreatorProperties creatorProperties;

    private BeanFactory beanFactory;

    @Test
    public void test() throws IOException {
//        defaultTemplateResolver.setTemplateMode("vue.html");
        templateEngine.setTemplateResolver(defaultTemplateResolver);
        Writer writer = new FileWriter("F:\\test\\aaa.html");
        String template = "vue";
        AbstractContext context = new Context();
        context.setVariable("name", "神的世界人不懂!");
        List<String> list = new ArrayList<>();
        list.add("狗");
        list.add("cat");
        list.add("xx");
        context.setVariable("array", list);
        templateEngine.process(template, context, writer);
    }

    @Test
    public void test2() {
        List<Tables> list = tablesService.list(null);
        System.out.println(list.toString());
    }

    @Test
    public void test3() {
        System.out.println(creatorProperties.toString());
    }

    /**
     * 测试 数据库访问与配置信息的绑定
     */
    @Test
    public void test4() {

        TablesServiceImpl bean = this.beanFactory.getBean(TablesServiceImpl.class);
        CreatorProperties properties = this.beanFactory.getBean(CreatorProperties.class);
        System.out.println(bean);
        System.out.println(properties);
    }

    /**
     * 获取指定库下的所有表
     */
    @Test
    public void test5() {

        /// 表与列信息获取
        Map<String, Object> map = new HashMap<>(16);
        getTableAndColumnInfo(map);
        ///
        /// 投值
        AbstractContext context = new Context();

        templateEngine.setTemplateResolver(defaultTemplateResolver);

        Set<String> keySet = map.keySet();
        keySet.forEach(key -> {
            try {
                context.setVariable("name", key);
                context.setVariable("applicationName", key);
                context.setVariable("keyMap", map);
                creatorCode(key, context, templateEngine);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    @Test
    public void creator() {
        /// 表与列信息获取
        Map<String, Object> map = new HashMap<>(16);
        getTableAndColumnInfo(map);
        templateEngine.setTemplateResolver(defaultTemplateResolver);
        /// 投值
        setContext(map);
    }

    private void setContext(Map<String, Object> map) {
        AbstractContext context = new Context();
        Set<String> keySet = map.keySet();
        keySet.forEach(key -> {
            try {
                context.setVariable("applicationName", creatorProperties.getApplicationName());
///             System.out.println("获取对象:--"+map.get(key));
                List<Columns> list = (List<Columns>) map.get(key);
                context.setVariable("keyMap", list);

                List<com.zx.mes.hyl.pagemodel.Columns> columnsList = new ArrayList<>();
                list.forEach(columns -> {
                    com.zx.mes.hyl.pagemodel.Columns cols = new com.zx.mes.hyl.pagemodel.Columns();
                    cols.setColumnName(columns.getColumnName());
                    cols.setDataType(columns.getDataType());
                    columnsList.add(cols);
                });
                System.out.println("表名列名:"+columnsList);
                /// 只要指定的类型数据
                context.setVariable("tableColumns", columnsList);
                String camel = NamingStrategy.underlineToCamel(key);
                /// 对应后端代码生成器controller beanName的对应生成规则
                context.setVariable("controller", camel);
                creatorCode(camel, context, templateEngine);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 代码生成
     *
     * @param name           生成文件名
     * @param context        上下文
     * @param templateEngine 引擎解析
     * @throws IOException 异常抛出
     */
    private void creatorCode(String name, AbstractContext context, SpringTemplateEngine templateEngine) throws IOException {
        String fileName = creatorProperties.getCodeDir() + "\\" + name;
        FileUtils.createFileOrDir(fileName);
        Writer writer = new FileWriter(fileName + "\\" + INDEX + SUFFIX);
        templateEngine.process(TEMPLATE, context, writer);
    }

    private void getTableAndColumnInfo(Map<String, Object> map) {

        QueryWrapper<Tables> wrapper = new QueryWrapper<>();
        String db = creatorProperties.getMysql().getDb();
        wrapper.lambda()
                .eq(Tables::getTableSchema, db).eq(Tables::getTableType, BASE_TABLE);
        List<Tables> tablesList = tablesService.list(wrapper);
        tablesList.forEach(table -> {
            ///System.out.println("--->" + table);
            QueryWrapper<Columns> wrapper2 = new QueryWrapper<>();
            wrapper2.lambda()
                    .eq(Columns::getTableSchema, db).eq(Columns::getTableName, table.getTableName());
            List<Columns> columnsList = this.columnsService.list(wrapper2);
            List<Columns> columns = new ArrayList<>(16);
            columnsList.forEach(column -> {

                ///System.out.println("==>字段名:" + column.getColumnName() + " 类型:" + column.getColumnType() + " 长度:" + column.getCharacterMaximumLength() + " 注释:" + column.getColumnComment());
                columns.add(column);
            });
            System.out.println("列名:"+columns);
            map.put(table.getTableName(), columns);
        });
    }


    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
