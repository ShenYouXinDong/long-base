package com.zx.mes.hyl;

import com.zx.mes.hyl.entity.sqlserver.Columns;
import com.zx.mes.hyl.entity.sqlserver.Table;
import com.zx.mes.hyl.mapper.sqlserver.ColumnMapper;
import com.zx.mes.hyl.mapper.sqlserver.TableMapper;
import com.zx.mes.hyl.properties.CreatorProperties;
import com.zx.mes.hyl.util.FileUtils;
import com.zx.mes.hyl.util.NamingStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.context.AbstractContext;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * slqServer code creator test
 *
 * @author 华云龙
 * @date 2018-11-21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ThymeleafSqlServerCreatorApplicationTest {

    /**
     * 指定生成vue文件的模板名称
     */
    private static final String TEMPLATE = "vue";

    /**
     * 生成vue文件的后缀
     */
    private static final String SUFFIX = ".vue";

    /**
     * 生成Vue文件的名称
     */
    private static final String INDEX = "index";
    @Autowired
    private SpringResourceTemplateResolver defaultTemplateResolver;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ColumnMapper columnMapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private TableMapper tableMapper;

    @Autowired
    private CreatorProperties creatorProperties;

    @Test
    public void test(){
        System.out.println(creatorProperties.toString());
        List<Columns> columnsList = columnMapper.getColumnsByTableName("tuser");
        System.out.println(columnsList.toString());

        List<Table> tableList = tableMapper.getTables();
        System.out.println(tableList);
    }

    @Test
    public void test2(){
        /// 表与列信息获取
        Map<String, Object> map = new HashMap<>(16);
        getTableAndColumnInfo(map);
        templateEngine.setTemplateResolver(defaultTemplateResolver);
        /// 投值
        setContext(map);
    }

    private void setContext(Map<String, Object> map) {
        AbstractContext context = new Context();
        Set<String> keySet = map.keySet();
        keySet.forEach(key -> {
            try {
                context.setVariable("applicationName", creatorProperties.getApplicationName());
///             System.out.println("获取对象:--"+map.get(key));
                List<Columns> list = (List<Columns>) map.get(key);
                context.setVariable("keyMap", list);

                List<com.zx.mes.hyl.pagemodel.Columns> columnsList = new ArrayList<>();
                list.forEach(columns -> {
                    com.zx.mes.hyl.pagemodel.Columns cols = new com.zx.mes.hyl.pagemodel.Columns();
                    cols.setColumnName(columns.getColumnName());
                    cols.setDataType(columns.getColumnTypeName());
                    columnsList.add(cols);
                });
                /// 只要指定的类型数据
                context.setVariable("tableColumns", columnsList);
                String camel = NamingStrategy.underlineToCamel(key);
                /// 对应后端代码生成器controller beanName的对应生成规则
                context.setVariable("controller", camel);
                creatorCode(camel, context, templateEngine);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 代码生成
     *
     * @param name           生成文件名
     * @param context        上下文
     * @param templateEngine 引擎解析
     * @throws IOException 异常抛出
     */
    private void creatorCode(String name, AbstractContext context, SpringTemplateEngine templateEngine) throws IOException {
        String fileName = creatorProperties.getCodeDir() + "\\" + name;
        FileUtils.createFileOrDir(fileName);
        Writer writer = new FileWriter(fileName + "\\" + INDEX + SUFFIX);
        templateEngine.process(TEMPLATE, context, writer);
    }

    private void getTableAndColumnInfo(Map<String, Object> map) {
        /// application.yml配置是哪个数据库,就生成哪个库下面的所有表的代码
        List<Table> tableList = tableMapper.getTables();

        tableList.forEach(table -> {
            ///System.out.println("--->" + table);
            List<Columns> columnsList = columnMapper.getColumnsByTableName(table.getTableName());

            List<Columns> columns = new ArrayList<>(16);
            columnsList.forEach(column -> {
                ///System.out.println("==>字段名:" + column.getColumnName() + " 类型:" + column.getColumnType() + " 长度:" + column.getCharacterMaximumLength() + " 注释:" + column.getColumnComment());
                columns.add(column);
            });
            map.put(table.getTableName(), columns);
        });
    }

}
