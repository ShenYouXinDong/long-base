package com.zx.mes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LongBaseDeemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LongBaseDeemoApplication.class, args);
	}
}
