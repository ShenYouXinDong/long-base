package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.AopConfig;
import com.zx.mes.hyl.math.MathCalculator;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * aop test
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class AopTest {

    @Test
    public void test() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);
        MathCalculator mathCalculator = (MathCalculator) context.getBean("mathCalculator");
        /// 正常测试
        mathCalculator.mod(10, 2);
        /// 测试异常
///        mathCalculator.mod(10,0);
    }
}
