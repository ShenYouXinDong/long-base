package com.zx.mes.hyl.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.stream.Stream;

/**
 * log math aspect
 * execution expression example:
 * public void com.zx.mes.hyl.math.MathCalculator.mod(int,int)
 * public void com.zx.mes.hyl.math.MathCalculator.*(..)
 * public * com.zx.mes.hyl.math.MathCalculator.*(..)
 *
 * JoinPoint 要放在参数的第一个位置
 *
 * @author 华云龙
 * @date 2018-10-26
 */
@Aspect
public class LogMathAspect {

    @Pointcut(value = "execution(public * com.zx.mes.hyl.math.MathCalculator.mod(int,int))")
    public void pointCut() {
    }

    @Before(value = "execution(public * com.zx.mes.hyl.math.MathCalculator.mod(int,int))")
    public void before(JoinPoint joinPoint) {
        System.out.println("方法执行之前打印参数:----------------");
        Stream.of(joinPoint.getArgs()).forEach(System.out::println);
        System.out.println("方法:" + joinPoint.getSignature().getName() + "执行之前执行!");
    }

    @After("pointCut()")
    public void after(JoinPoint joinPoint) {
        System.out.println("方法:" + joinPoint.getSignature().getName() + "执行之后!");
    }

    @AfterReturning(value = "pointCut()", returning = "result")
    public void logReturn(JoinPoint joinPoint, Object result) {
        System.out.println("方法:" + joinPoint.getSignature().getName() + "的返回值:" + result);
    }

    @AfterThrowing(value = "pointCut()", throwing = "exception")
    public void logException(JoinPoint joinPoint, Exception exception) {
        System.out.println("下面打印异常:------------------------");
        System.out.println(joinPoint.getSignature().getName() + ":" + exception.getMessage());
    }
}
