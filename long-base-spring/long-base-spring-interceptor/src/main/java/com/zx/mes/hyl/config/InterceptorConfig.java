package com.zx.mes.hyl.config;

import com.zx.mes.hyl.interceptor.MyInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * interceptor config
 *
 * @author 华云龙
 * @date 2019-2-13
 */
@Configuration
public class InterceptorConfig {

    @Bean
    public MyInterceptor myInterceptor(){
        return new MyInterceptor();
    }
}
