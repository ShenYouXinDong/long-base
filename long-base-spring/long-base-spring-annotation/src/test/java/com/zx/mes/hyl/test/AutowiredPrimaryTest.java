package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.AutowiredPrimaryConfig;
import com.zx.mes.hyl.service.BaseServiceImpl;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * autowired primary test
 * 测试指定优先级别进行注入
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class AutowiredPrimaryTest {

    @Test
    public void test() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AutowiredPrimaryConfig.class);
        BaseServiceImpl baseService = (BaseServiceImpl) context.getBean("baseService");
        baseService.test();
    }
}
