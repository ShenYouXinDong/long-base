package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ConditionClassConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * condition class test
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class ConditionClassTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionClassConfig.class);
        String[] names = context.getBeanDefinitionNames();
        Stream.of(names).forEach(System.out::println);
    }
}
