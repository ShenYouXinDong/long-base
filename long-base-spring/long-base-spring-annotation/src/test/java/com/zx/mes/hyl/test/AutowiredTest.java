package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.AutowiredConfig;
import com.zx.mes.hyl.service.BaseServiceImpl;
import com.zx.mes.hyl.service.BaseServiceImplQualifier;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * autowired test
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class AutowiredTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(AutowiredConfig.class);
        BaseServiceImpl baseService=(BaseServiceImpl) context.getBean("baseService");
        baseService.test();

        BaseServiceImplQualifier baseServiceQualifier=(BaseServiceImplQualifier) context.getBean("baseServiceQualifier");
        baseServiceQualifier.test();
    }
}
