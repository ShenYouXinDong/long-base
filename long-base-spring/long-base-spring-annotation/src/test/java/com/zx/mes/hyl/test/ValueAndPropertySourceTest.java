package com.zx.mes.hyl.test;

import com.zx.mes.hyl.bean.DataSource;
import com.zx.mes.hyl.config.ValueAndPropertySourceConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * @Value and @PropertySource test
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class ValueAndPropertySourceTest {

    @Test
    public void test (){
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(ValueAndPropertySourceConfig.class);
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
        DataSource dataSource=(DataSource) context.getBean("com.zx.mes.hyl.bean.DataSource");
        System.out.println(dataSource.getPassword());

        System.out.println(dataSource.toString());
    }
}
