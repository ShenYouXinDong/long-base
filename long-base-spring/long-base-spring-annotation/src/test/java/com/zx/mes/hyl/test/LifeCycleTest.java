package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.LifeCycleConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * life cycle test
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class LifeCycleTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LifeCycleConfig.class);
        context.close();

    }
}
