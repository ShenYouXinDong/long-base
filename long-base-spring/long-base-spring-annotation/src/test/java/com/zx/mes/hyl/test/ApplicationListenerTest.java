package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ApplicationListenerConfig;
import org.junit.Test;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * ApplicationListener test
 *
 * @author 华云龙
 * @date 2018-11-6
 */
public class ApplicationListenerTest {

    @Test
    public void test() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationListenerConfig.class);
        ApplicationEvent event = new ApplicationEvent("神一般的世界") {
            @Override
            public Object getSource() {
                return super.getSource();
            }
        };
        context.publishEvent(event);
        context.close();
    }
}
