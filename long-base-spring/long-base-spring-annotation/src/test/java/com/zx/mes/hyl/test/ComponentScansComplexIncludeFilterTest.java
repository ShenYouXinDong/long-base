package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ComponentScansComplexIncludeFilterConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * componentScans complex includeFilter test
 *
 * @author 华云龙
 * @date 2018-10-25
 */
public class ComponentScansComplexIncludeFilterTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ComponentScansComplexIncludeFilterConfig.class);
        String[] names = context.getBeanDefinitionNames();
        Stream.of(names).forEach(System.out::println);
    }
}
