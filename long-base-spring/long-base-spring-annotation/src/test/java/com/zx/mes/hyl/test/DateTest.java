package com.zx.mes.hyl.test;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

/**
 * date convert
 *
 * @author 华云龙
 * @date 2018-10-25
 */
public class DateTest {

    @Test
    public void test() {
//        System.out.println(new Date().getTime());
//        System.out.println(LocalDateTime.now());
//        System.out.println(new Date(new Date().getYear()));
//        System.out.println(new Date().getDay());


//        System.out.println(new Date().getTime()/1000/3600/24 + 25932);


//test2();
        Calendar calendar = Calendar.getInstance();
        calendar.set(1900, 0, 1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(1970, 0, 1);

        System.out.println(calendar.getTimeInMillis());
        System.out.println(calendar2.getTimeInMillis());
        long cc = calendar2.getTimeInMillis() - calendar.getTimeInMillis();
        double c1 = cc / 1000;
        double c2 = c1 / 3600;
        double re = c2 / 24;
        System.out.println(re);
    }

    public void test2() {
        int sum = 0;
        for (int year = 1900; year <= 1970; year++) {
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
                sum = sum + 366;
            } else {
                sum = sum + 365;
            }
        }
        System.out.println("天数:" + sum);
    }


    @Test
    public void test3() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(1900, 0, 1, 0, 0);
        System.out.println(calendar.getTime());

        Calendar calendar2 = Calendar.getInstance();
//        calendar2.setTime(new Date());
//        calendar2.set(2017, 11, 1, 1, 29, 59);
        calendar2.setTimeInMillis(System.currentTimeMillis());
        System.out.println(calendar2.getTime());

        long cc = calendar2.getTimeInMillis() - calendar.getTimeInMillis();
        double c1 = cc / 1000;
        double c2 = c1 / 3600;
        double re = c2 / 24;
        System.out.println(re+2);


    }




    @Test
    public void test4() {
        double v = 43070.062489121 - 43070.06193287037;
        System.out.println(v * 24 * 3600 + "秒");
    }

    @Test
    public void test5() {
        LocalDateTime time = LocalDateTime.of(2017, 12, 1, 1, 29, 59);
        long milli = time.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println(milli);

        System.out.println(new Date(milli));

        LocalDateTime time2 = LocalDateTime.of(1900, 1, 1, 0, 0, 0);
        long milli2 = time2.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        double cc = milli - milli2;
        System.out.println(cc / 1000 / 3600 / 24);
        System.out.println(javaToVBDateDays(2017, 12, 1, 1, 29, 59));
    }

    /**
     * java 时间转成 vb 时间  换算完成day
     *
     * @param year       四位 如 :2018
     * @param month      月份 1-12
     * @param dayOfMonth 天 1-31
     * @param hour       小时 0-24
     * @param minute     分钟 0-60
     * @param second     秒 0-60
     * @return 返回天数 如:43070.06248842592
     */
    public double javaToVBDateDays(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        LocalDateTime time = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
        long milli = time.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        LocalDateTime timeZone = LocalDateTime.of(1900, 1, 1, 0, 0, 0);
        long milliZone = timeZone.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        double re = milli - milliZone;
        return (re / 1000 / 3600 / 24) + 2;
    }


    @Test
    public void test6() {
        System.out.println(javaToVBDateDays(LocalDateTime.now()));
    }

    /**
     * @param date LocalDateTime
     * @return 返回天数 如:43070.06248842592
     */
    public double javaToVBDateDays(LocalDateTime date) {
        int year, month, dayOfMonth, hour, minute, second;
        year = date.getYear();
        month = date.getMonthValue();
        dayOfMonth = date.getDayOfMonth();
        hour = date.getHour();
        minute = date.getMinute();
        second = date.getSecond();

        LocalDateTime time = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
        long milli = time.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        LocalDateTime timeZone = LocalDateTime.of(1900, 1, 1, 0, 0, 0);
        long milliZone = timeZone.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        double re = milli - milliZone;
        return (re / 1000 / 3600 / 24) + 2;
    }


    /**
     *
     * @param times 毫秒
     * @return 返回天数 如:43070.06248842592
     */
    public double javaToVBDateDays(long times) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1900, 0, 1, 0, 0);
        Calendar calendar2 = Calendar.getInstance();

        calendar2.setTimeInMillis(times);

        long cc = calendar2.getTimeInMillis() - calendar.getTimeInMillis();
        double c1 = cc / 1000;
        double c2 = c1 / 3600;
        double re = c2 / 24;
        return re;
    }
}
