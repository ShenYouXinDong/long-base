package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ComponentScanComplexExcludeFilterConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * componentScan complex excludeFilter test
 * @author 华云龙
 * @date 2018-10-25
 */
public class ComponentScanComplexExcludeFilterTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ComponentScanComplexExcludeFilterConfig.class);
        String[] names = context.getBeanDefinitionNames();
        Stream.of(names).forEach(System.out::println);
    }
}
