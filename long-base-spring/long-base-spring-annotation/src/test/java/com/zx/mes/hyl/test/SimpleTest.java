package com.zx.mes.hyl.test;

import com.zx.mes.hyl.bean.Dog;
import com.zx.mes.hyl.bean.Person;
import com.zx.mes.hyl.config.ComponentScanConfig;
import com.zx.mes.hyl.config.SimpleConfig;
import com.zx.mes.hyl.service.BaseServiceImpl;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * 各种生成bean方式测试
 *
 * @author 华云龙
 * @date 2018-10-24
 */
public class SimpleTest {

    /**
     * SimpleConfig.class
     * @throws InterruptedException
     */
    @Test
    public void test() throws InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SimpleConfig.class);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Stream.of(beanDefinitionNames).forEach(System.out::println);
        System.out.println("容器创建完成!");
        Thread.sleep(10000);
        Dog dog = (Dog) context.getBean("dog");
        System.out.println(dog.toString());
    }

    @Test
    public void test2() throws InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ComponentScanConfig.class);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Stream.of(beanDefinitionNames).forEach(System.out::println);

        Thread.sleep(2000);
        BaseServiceImpl serviceImpl = (BaseServiceImpl) context.getBean("baseServiceImpl");
        serviceImpl.test();
    }
}
