package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.SetSpringContainerConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * spring container di  test
 *
 * @author 华云龙
 * @date 2018-10-29
 */
public class SetSpringContainerTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SetSpringContainerConfig.class);
        context.close();

    }

}
