package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.FactoryBeanConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * FactoryBean test
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class FactoryBeanTest {

    @Test
    public void test() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(FactoryBeanConfig.class);
        String[] names = context.getBeanDefinitionNames();
        Stream.of(names).forEach(System.out::println);

        Object bean = context.getBean("personFactoryBean");
        System.out.println("personFactoryBean:"+bean.getClass());
        /// 想获取工厂bean加&
        Object factoryBean = context.getBean("&personFactoryBean");
        System.out.println("&personFactoryBean:"+factoryBean.getClass());
    }
}
