package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ConditionMethodConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ConditionContext;

import java.util.stream.Stream;

/**
 * condition method test
 * @author 华云龙
 * @date 2018-10-26
 */
public class ConditionMethodTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionMethodConfig.class);
        String[] names = context.getBeanDefinitionNames();
        Stream.of(names).forEach(System.out::println);
    }
}
