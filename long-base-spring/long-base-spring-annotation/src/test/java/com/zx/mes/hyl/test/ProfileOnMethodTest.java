package com.zx.mes.hyl.test;

import com.zx.mes.hyl.config.ProfileOnMethodConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * Profile on method test
 *
 * @author 华云龙
 * @date 2018-10-29
 */
public class ProfileOnMethodTest {

    @Test
    public void test(){
///        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProfileOnMethodConfig.class);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("test");
        context.register(ProfileOnMethodConfig.class);
        context.refresh();
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
