package com.zx.mes.hyl.test.aware;

import com.zx.mes.hyl.config.aware.AwareConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * Aware interface test
 *
 * @author 华云龙
 * @date 2019-1-10
 */
public class AwareTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AwareConfig.class);
        ConfigurableEnvironment environment = context.getEnvironment();
        //System.out.println(environment);
    }
}
