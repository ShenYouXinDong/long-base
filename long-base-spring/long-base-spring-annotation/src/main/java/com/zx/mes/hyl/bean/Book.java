package com.zx.mes.hyl.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * boot impl InitializingBean,DisposableBean
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class Book implements InitializingBean, DisposableBean {
    @Override
    public void destroy() throws Exception {
        System.out.println("book DisposableBean destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("book InitializingBean init");
    }
}
