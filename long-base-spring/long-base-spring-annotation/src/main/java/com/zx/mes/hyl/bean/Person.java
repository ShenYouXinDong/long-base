package com.zx.mes.hyl.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * person 实体类
 *
 * @author 华云龙
 * @date 2018-10-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class Person {
    private String name;
    private Integer age;
    private Date birthDate;
}
