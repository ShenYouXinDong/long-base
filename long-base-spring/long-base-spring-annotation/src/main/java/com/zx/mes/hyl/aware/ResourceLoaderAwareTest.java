package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

/**
 * 实现ResourceLoaderAware 接口
 *
 * @author 华云龙
 * @date 2019-1-9
 */
@Component
@Slf4j
public class ResourceLoaderAwareTest implements ResourceLoaderAware {

    private ResourceLoader resourceLoader;

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
        log.debug("ResourceLoaderAware测试:"+resourceLoader);
        ClassLoader classLoader = resourceLoader.getClassLoader();
        log.debug("ResourceLoaderAware测试:"+classLoader);
    }
}
