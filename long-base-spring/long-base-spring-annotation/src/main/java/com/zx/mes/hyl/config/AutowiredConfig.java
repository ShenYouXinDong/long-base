package com.zx.mes.hyl.config;

import com.zx.mes.hyl.dao.BaseDao;
import com.zx.mes.hyl.service.BaseServiceImpl;
import com.zx.mes.hyl.service.BaseServiceImplQualifier;
import org.springframework.context.annotation.Bean;

/**
 * autowired config
 * spring autowired
 * 1.默认是按类型装配,当一个类型有多个实例时,按装配属性来装配
 * 2.@Qu
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class AutowiredConfig {

    @Bean
    public BaseDao baseDao(){
        BaseDao baseDao= new BaseDao();
        baseDao.setLabel("1");
        return baseDao;
    }

    @Bean
    public BaseDao baseDao2(){
        BaseDao baseDao= new BaseDao();
        baseDao.setLabel("2");
        return baseDao;
    }

    /**
     * @Autowired
     * private BaseDao baseDao;
     * 所以使用的是 label值为一的bean
     * @return baseServiceImpl
     */
    @Bean
    public BaseServiceImpl baseService(){
        return new BaseServiceImpl();
    }

    /**
     * private BaseDao baseDao @Qualifier 指定实例baseDao2注入
     * @return
     */
    @Bean
    public BaseServiceImplQualifier baseServiceQualifier(){
        return new BaseServiceImplQualifier();
    }
}
