package com.zx.mes.hyl.bean;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author 华云龙
 * @date 2018-10-24
 */
@Data
@Accessors(chain = true)
@ToString
public class Cat {
    private String name;
    private Integer age;

    public Cat(){
        System.out.println("无参构造执行!");
    }

    public Cat(String name, Integer age) {
        this.name = name;
        this.age = age;
        System.out.println("有参构造执行!");
    }
}
