package com.zx.mes.hyl.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * componentScan  complex exclude filter config
 * @author 华云龙
 * @date 2018-10-24
 */
@Configuration
@ComponentScan(basePackages = "com.zx.mes.hyl",excludeFilters = {
        @ComponentScan.Filter(type=FilterType.ANNOTATION,classes = {Controller.class, Service.class})
})
public class ComponentScanComplexExcludeFilterConfig {
}
