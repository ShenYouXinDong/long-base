package com.zx.mes.hyl.bean;

import lombok.NoArgsConstructor;

/**
 * life cycle test
 *
 * @author 华云龙
 * @date 2018-10-26
 */
@NoArgsConstructor
public class Car {

    public void init() {
        System.out.println("car .....init");
    }

    public void destroy() {
        System.out.println("car ......destroy");
    }
}
