package com.zx.mes.hyl.registrar;

import com.zx.mes.hyl.bean.Dog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * ImportBeanDefinitionRegistrar impl
 *
 * @author 华云龙
 * @date 2018-10-25
 */
public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    /**
     * 做一些简单的判断,spring boot 中使用最多的是ImportSelector,要判断可以使用Condition注解
     *
     * @param importingClassMetadata 当前类的注解信息
     * @param registry               将所有需要添加到容器中的bean,手工注册到容器里
     */
    @Override
    @SuppressWarnings("NullableProblems")
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        boolean b = registry.containsBeanDefinition("com.zx.mes.hyl.bean.Cat");
        if (b) {
            BeanDefinition benDefinition = new RootBeanDefinition(Dog.class);
            /// 定义一个bean,指定一个bean name
            registry.registerBeanDefinition("custom-dog", benDefinition);
        }
    }
}
