package com.zx.mes.hyl.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * componentScan 方法注册bean
 *
 * @author 华云龙
 * @date 2018-10-24
 */
@Configuration
@ComponentScan("com.zx.mes.hyl")
public class ComponentScanConfig {

}
