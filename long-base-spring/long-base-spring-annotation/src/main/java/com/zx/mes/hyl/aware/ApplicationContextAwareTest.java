package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * ApplicationContextAware test
 *
 * @author 华云龙
 * @date 2019-1-10
 */
@Component
@Slf4j
public class ApplicationContextAwareTest implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;
        log.debug("ApplicationContextAware测试:"+applicationContext);
    }
}
