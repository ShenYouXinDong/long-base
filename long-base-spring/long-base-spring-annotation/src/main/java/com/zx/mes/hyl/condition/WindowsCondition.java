package com.zx.mes.hyl.condition;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * window condition
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class WindowsCondition implements Condition {
    /**
     * 一般根据上下文中环境属性,或必需存在的bean进行判断
     *
     * @param context  能作为判断依据的上下文
     * @param metadata 类的注解信息
     * @return true / false
     */
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        /// beanFactory
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        /// 类加载器
        ClassLoader classLoader = context.getClassLoader();
        /// 环境
        Environment environment = context.getEnvironment();
        /// bean 定义注册
        BeanDefinitionRegistry registry = context.getRegistry();
        /// 资源加载器
        ResourceLoader resourceLoader = context.getResourceLoader();
        String property = environment.getProperty("os.name");
        if (property.contains("Windows")) {
            return true;
        }
        return false;
    }
}
