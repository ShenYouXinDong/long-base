package com.zx.mes.hyl.config;

import com.zx.mes.hyl.listener.AnnotationListener;
import com.zx.mes.hyl.listener.MyApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * ApplicationListener  config
 * @author 华云龙
 * @date 2018-11-6
 */
@Configuration
@Import({MyApplicationListener.class, AnnotationListener.class})
public class ApplicationListenerConfig {


}
