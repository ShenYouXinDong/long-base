package com.zx.mes.hyl.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;

/**
 * annotation listener
 *
 * @author 华云龙
 * @date 2018-11-6
 */
public class AnnotationListener {

    @EventListener(value = ApplicationEvent.class)
    public void listener(ApplicationEvent event){
        System.out.println("注解监听:"+event);
    }
}
