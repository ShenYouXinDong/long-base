package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * EmbeddedValueResolverAware test
 *
 * @author 华云龙
 * @date 2019-1-9
 */
@Slf4j
@Component
public class EmbeddedValueResolverAwareTest implements EmbeddedValueResolverAware {
    private StringValueResolver stringValueResolver;
    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        this.stringValueResolver = resolver;
        log.debug("EmbeddedValueResolverAware测试(StringValueResolver):"+resolver);
    }
}
