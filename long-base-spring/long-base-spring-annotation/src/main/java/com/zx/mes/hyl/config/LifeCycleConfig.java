package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Book;
import com.zx.mes.hyl.bean.Car;
import com.zx.mes.hyl.bean.Pig;
import com.zx.mes.hyl.processor.MyBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * life cycle config
 * 1.如xml文件配置的一样,配置init-method和destroy-method,指定类中的方法
 * 2.spring 中 InitializingBean,DisposableBean
 * 3.BeanPostProcessor bean后置处理器
 * postProcessBeforeInitialization方法调用时机:如下(查看源码可以看出下面的顺序)
 * Apply this BeanPostProcessor to the given new bean instance <i>before</i> any bean
 * initialization callbacks (like InitializingBean's {@code afterPropertiesSet}
 * or a custom init-method)
 * 4.JSR250
 *         @PostConstruct：在bean创建完成并且属性赋值完成；来执行初始化方法
 * 		   @PreDestroy：在容器销毁bean之前通知我们进行清理工作
 *
 * @author 华云龙
 * @date 2018-10-26
 */
@Configuration
@Import({Book.class, MyBeanPostProcessor.class, Pig.class})
public class LifeCycleConfig {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public Car car() {
        return new Car();
    }


}
