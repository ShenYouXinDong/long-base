package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.PersonFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 实现FactoryBean 然后@Bean配置
 * @author 华云龙
 * @date 2018-10-26
 */
@Configuration
public class FactoryBeanConfig {

    @Bean
    public PersonFactoryBean personFactoryBean(){
        return new PersonFactoryBean();
    }
}
