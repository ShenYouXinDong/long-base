package com.zx.mes.hyl.config;

import com.zx.mes.hyl.container.Color;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/***
 * spring container config(spring 容器注入自定义容器)
 *
 * @author 华云龙
 * @date 2018-10-29
 */
@Configuration
@Import(Color.class)
public class SetSpringContainerConfig {


}
