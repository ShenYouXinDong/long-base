package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * profile 根据当前环境进行,动态激活和切换一系列组件的功能
 * 不加注解@Profile时,三个bean都会被添加至容器中
 * 若想默认加载哪一个环境下的bean @Profile("default")
 *
 * @author 华云龙
 * @date 2018-10-29
 */
@Configuration
public class ProfileOnMethodConfig {

    @Profile("test")
    @Bean
    public DataSource dataSourceTest(){
        DataSource dataSource = new DataSource();
        dataSource.setUsername("test");
        return dataSource;
    }

    @Profile("dev")
    @Bean
    public DataSource dataSourceDev(){
        DataSource dataSource = new DataSource();
        dataSource.setUsername("test");
        return dataSource;
    }

    @Profile("prod")
    @Bean
    public DataSource dataSourceProd(){
        DataSource dataSource = new DataSource();
        dataSource.setUsername("test");
        return dataSource;
    }

}
