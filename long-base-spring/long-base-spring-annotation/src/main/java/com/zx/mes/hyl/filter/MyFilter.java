package com.zx.mes.hyl.filter;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * @author 华云龙
 * @date 2018
 */
public class MyFilter implements TypeFilter {
    /**
     *
     * @param metadataReader the metadata reader for the target class (在扫描包下所有的类,都会一个一个的传递进来进行判断)
     * @param metadataReaderFactory
     * @return
     * @throws IOException
     */
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        /// 获取当前类注解信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        /// 获取当前类正在扫描类的信息
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        /// org.springframework.core.type.classreading.SimpleMetadataReader
        Class<? extends MetadataReader> aClass = metadataReader.getClass();
        /// 获取当前类地址
        Resource resource = metadataReader.getResource();
//        System.out.println("annotationMetadata ----->");
//        Stream.of(annotationMetadata.getAnnotationTypes().toArray()).forEach(System.out::println);

//        System.out.println("classMetadata------------>");
//        System.out.println("classMetadata.getClassName()---->:"+classMetadata.getClassName());
//        System.out.println("classMetadata.getSuperClassName()---->:"+classMetadata.getSuperClassName());
//
//        System.out.println("aClass---------------->");
//        System.out.println("aClass.getName()"+aClass.getName());
//
//        System.out.println("aClass.getAnnotations()---------->");
//        Stream.of(aClass.getAnnotations()).forEach(System.out::println);
//
//        System.out.println("resource------------------>");
//        System.out.println(resource.getURL());
        System.out.println(classMetadata.getClassName());
        if (classMetadata.getClassName().contains("Controller")) {
            return true;
        }

        return false;
    }
}
