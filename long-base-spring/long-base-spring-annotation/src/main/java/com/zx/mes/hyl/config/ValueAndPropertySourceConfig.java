package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.DataSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Value 结合 @PropertySource
 *
 * @author 华云龙
 * @date 2018-10-27
 */
@Configuration
@Import(DataSource.class)
public class ValueAndPropertySourceConfig {
}
