package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Person;
import com.zx.mes.hyl.condition.LinusCondition;
import com.zx.mes.hyl.condition.WindowsCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * condition method config
 *
 * @author 华云龙
 * @date 2018-10-26
 */
@Configuration
public class ConditionMethodConfig {

    @Conditional(WindowsCondition.class)
    @Bean("bill")
    public Person person() {
        return new Person("比尔盖", 60, new Date());
    }

    @Conditional(LinusCondition.class)
    @Bean("linus")
    public Person person2() {
        return new Person("Linus", 45, new Date());
    }
}
