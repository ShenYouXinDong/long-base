package com.zx.mes.hyl.bean;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 狗实体类
 *
 * @author 华云龙
 * @date 2018-10-24
 */
@Data
@Accessors(chain = true)
@ToString
public class Dog {
    private String name;
    private Integer age;
    public Dog(){
        System.out.println("无参构造执行!");
    }

    public Dog(String name,Integer age){
        this.name = name;
        this.age = age;
        System.out.println("有参构造执行了!");
    }
}
