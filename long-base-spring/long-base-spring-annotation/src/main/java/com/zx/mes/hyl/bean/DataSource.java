package com.zx.mes.hyl.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * dataSource 模拟
 * @Value 三种使用形式
 * 1.基本值
 * 2.spring el表达式
 * 3.${}
 * 4.可能会出现中文乱码(解决,设置编码格式)
 *
 * @author 华云龙
 * @date 2018-10-27
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@PropertySource(value = {"classpath:/jdbc.properties"},encoding = "UTF-8")
public class DataSource {

    @Value("张三")
    private String username;
    @Value("${jdbc.password}")
    private String password;
    private String url;
    @Value("#{20-2}")
    private Integer connection;
}
