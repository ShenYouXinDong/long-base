package com.zx.mes.hyl.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * componentScans complex includeFilter config
 *
 * @author 华云龙
 * @date 2018-10-25
 */
@Configuration
@ComponentScans(value = {
        @ComponentScan(value = "com.zx.mes.hyl", useDefaultFilters = false,includeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Controller.class)
        }),
        @ComponentScan(value = "com.zx.mes.hyl", includeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Service.class, Repository.class})
        },useDefaultFilters = false)
})
public class ComponentScansComplexIncludeFilterConfig {
}
