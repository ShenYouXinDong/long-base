package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Dog;
import com.zx.mes.hyl.bean.Person;
import com.zx.mes.hyl.condition.LinusCondition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * condition class config
 * 与method作用一致,只是范围不一样
 * 有无condition的变化
 *
 * @author 华云龙
 * @date 2018-10-26
 */
@Configuration
@Conditional(LinusCondition.class)
public class ConditionClassConfig {

    @Bean
    public Person person() {
        return new Person();
    }

    @Bean
    public Dog dog() {
        return new Dog();
    }
}
