package com.zx.mes.hyl.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * JSR250 @PostConstruct @PreDestroy
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class Pig {
    private String name;
    private Integer age;

    @PostConstruct
    public void init(){
        System.out.println("@PostConstruct init");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("@PreDestroy destroy");
    }

}
