package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Computer;
import com.zx.mes.hyl.filter.MyFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * componentScan complex includeFilter config
 * @author 华云龙
 * @date 2018-10-25
 */
@Configuration
@ComponentScan(value = "com.zx.mes.hyl",includeFilters = {
        @ComponentScan.Filter(type= FilterType.ANNOTATION,classes = Controller.class),
        @ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE,classes = Computer.class),
        @ComponentScan.Filter(type=FilterType.CUSTOM,classes = MyFilter.class)
},useDefaultFilters = false)
public class ComponentScanComplexIncludeFilterConfig {


}
