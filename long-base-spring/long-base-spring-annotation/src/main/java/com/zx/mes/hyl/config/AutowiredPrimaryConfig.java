package com.zx.mes.hyl.config;

import com.zx.mes.hyl.dao.BaseDao;
import com.zx.mes.hyl.service.BaseServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * autowired primary 当有多个同类型实例时,优先注入哪一个
 *
 * @author 华云龙
 * @date 2018-10-27
 */
@Configuration
public class AutowiredPrimaryConfig {

    @Bean
    public BaseDao baseDao(){
        BaseDao baseDao= new BaseDao();
        baseDao.setLabel("1");
        return baseDao;
    }

    /**
     * @Primary 优先使用 同类型哪个实例进行注入
     * @return baseDao
     */
    @Primary
    @Bean
    public BaseDao baseDao2(){
        BaseDao baseDao= new BaseDao();
        baseDao.setLabel("2");
        return baseDao;
    }

    @Bean
    public BaseServiceImpl baseService(){
        return new BaseServiceImpl();
    }

}
