package com.zx.mes.hyl.service;

import com.zx.mes.hyl.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * baseDao @Qualifier 指定实例注入
 *
 * @author 华云龙
 * @date 2018－10－27
 */
@Service
public class BaseServiceImplQualifier {

    @Qualifier("baseDao2")
    @Autowired
    private BaseDao baseDao;

    public void test() {
        System.out.println("baseServiceImpl执行!");
        baseDao.test();
    }
}
