package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * BeanFactoryAware test
 *
 * @author 华云龙
 * @date 2019-1-9
 */
@Slf4j
@Component
public class BeanFactoryAwareTest implements BeanFactoryAware {

    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
        log.debug("BeanFactoryAware测试:"+beanFactory);
        ConfigurableListableBeanFactory factory = (ConfigurableListableBeanFactory) beanFactory;
        String[] beanDefinitionNames = factory.getBeanDefinitionNames();
        log.debug("------------- beanFactory里的beanDefinitionName -------------");
        for (int i=0;i<beanDefinitionNames.length;i++) {
            log.debug(beanDefinitionNames[i].toString());
        }

    }
}
