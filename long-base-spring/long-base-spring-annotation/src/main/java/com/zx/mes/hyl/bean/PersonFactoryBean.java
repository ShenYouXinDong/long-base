package com.zx.mes.hyl.bean;

import org.springframework.beans.factory.FactoryBean;

import java.util.Date;

/**
 * FactoryBean 实现
 * @author 华云龙
 * @date 2018-10-26
 */
public class PersonFactoryBean implements FactoryBean{

    @Override
    public Object getObject() throws Exception {
        return new Person("神被工厂bean创建",20,new Date());
    }

    @Override
    public Class<?> getObjectType() {
        return Person.class;
    }

    /**
     * 根据boolean返回单例还是原型
     * @return false / true
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}
