package com.zx.mes.hyl.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * linux condition
 *
 * @author 华云龙
 * @date 2018-10-26
 */
public class LinusCondition implements Condition {

    private static final String LINUX = "Linux";

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment environment = context.getEnvironment();
        String property = environment.getProperty("os.name");
        assert property != null;
        return property.contains(LINUX);
    }
}
