package com.zx.mes.hyl.config.aware;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author 华云龙
 * @date 2019-1-10
 */
@Configuration
@ComponentScan(basePackages = {"com.zx.mes.hyl.aware","com.zx.mes.hyl.postprocess"})
public class AwareConfig {
}
