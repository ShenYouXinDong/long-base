package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.stereotype.Component;

/**
 * BeanClassLoaderAware test
 * @author 华云龙
 * @date 2019-1-10
 */
@Slf4j
@Component
public class BeanClassLoaderAwareTest implements BeanClassLoaderAware {

    private ClassLoader classLoader;

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
        log.debug("BeanClassLoaderAware测试:"+classLoader);
    }
}
