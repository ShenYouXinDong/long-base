package com.zx.mes.hyl.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * ApplicationListener  impl
 *
 * @author 华云龙
 * @date 2018-11-6
 */
public class MyApplicationListener implements ApplicationListener<ApplicationEvent> {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("接收事件:" + event);
    }
}
