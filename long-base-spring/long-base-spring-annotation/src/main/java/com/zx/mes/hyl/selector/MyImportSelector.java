package com.zx.mes.hyl.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * 自定义哪些类应被 @Import
 * (如果该接口的实现类同时实现EnvironmentAware， BeanFactoryAware ，BeanClassLoaderAware或者ResourceLoaderAware，
 * 那么在调用其selectImports方法之前先调用上述接口中对应的方法，
 * 如果需要在所有的@Configuration处理完在导入时可以实现DeferredImportSelector接口)
 * 看spring boot 源码:一般作直接导入类使用,上述()内很少这样使用
 * @author 华云龙
 * @date 2018-10-25
 */
public class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        System.out.println("-----------------------------MyImportSelector.class 执行!-----------------------------");
        /// 获取ImportConfig.class的所有注解类型
        importingClassMetadata.getAnnotationTypes().forEach(System.out::println);
        MultiValueMap<String, Object> valueMap = importingClassMetadata.getAllAnnotationAttributes("org.springframework.context.annotation.Import");
        ///
///        MultiValueMap<String, Object> value = importingClassMetadata.getAllAnnotationAttributes("value");
        Set<String> keySet = valueMap.keySet();
        keySet.forEach((key) -> {
            List<Object> list = valueMap.get(key);
            Class[] clazz = (Class[]) list.get(0);
            Stream.of(clazz).forEach(System.out::println);
        });
        System.out.println("-----------------------------MyImportSelector.class 执行结束!-----------------------------");
        return new String[0];
    }
}
