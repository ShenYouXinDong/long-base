package com.zx.mes.hyl.service;

import com.zx.mes.hyl.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaseServiceImpl {

    @Autowired
    private BaseDao baseDao;

    public void test() {
        System.out.println("baseServiceImpl执行!");
        baseDao.test();
    }
}
