package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Dog;
import com.zx.mes.hyl.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * 简单bean配置
 *
 * @author 华云龙
 * @date 2018-10-24
 */
@Configuration
public class SimpleConfig {

    /**
     * simple bean
     *
     * @return Person.class
     */
    @Bean
    public Person person() {
        return new Person();
    }

    @Bean("myDog")
    public Dog dog() {
        return new Dog();
    }

    /**
     * 懒加载,还有单例,原型实例化时的区别
     * lazy 只作用于单例
     * 原型模式本就是每次获取时才实例化一次
     *
     * @return Dog.class
     */
    @Lazy
    @Bean("dog")
    public Dog dog2() {
        return new Dog("笑天吠", 3);
    }
}
