package com.zx.mes.hyl.controller;

import org.springframework.stereotype.Controller;

/**
 * baseController
 * @author 华云龙
 * @date 2018-10-25
 */
@Controller
public class BaseController {

    public void test(){
        System.out.println("controller test!");
    }
}
