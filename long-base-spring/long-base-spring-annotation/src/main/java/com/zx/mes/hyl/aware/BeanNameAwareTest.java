package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

/**
 * BeanNameAware test
 *
 * @author 华云龙
 * @date 2019-1-10
 */
@Component
@Slf4j
public class BeanNameAwareTest implements BeanNameAware {

    @Override
    public void setBeanName(String name) {
        log.debug("BeanNameAware测试:"+name);
    }
}
