package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.stereotype.Component;

/**
 * ImportAware test
 *
 * @author 华云龙
 * @date 20189-1-10
 */
@Slf4j
@Component
public class ImportAwareTest implements ImportAware {

    private AnnotationMetadata annotationMetadata;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        this.annotationMetadata = importMetadata;
        log.debug("ImportAware测试:"+importMetadata);
    }
}
