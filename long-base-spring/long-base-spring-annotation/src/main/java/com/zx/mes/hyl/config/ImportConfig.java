package com.zx.mes.hyl.config;

import com.zx.mes.hyl.bean.Cat;
import com.zx.mes.hyl.bean.Computer;
import com.zx.mes.hyl.registrar.MyImportBeanDefinitionRegistrar;
import com.zx.mes.hyl.selector.MyImportSelector;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * import class
 * 生成的id是类的全路径
 * ImportBeanDefinitionRegister.class (source code analysis)
 * 方法栈:
 *      refresh();
 *          /// Invoke factory processors registered as beans in the context.
 *           invokeBeanFactoryPostProcessors(beanFactory);
 *              /// getBeanFactoryPostProcessors() (集合)获取所有实现PostProcessor
 *              PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors(beanFactory, getBeanFactoryPostProcessors());
 *                  /// org.springframework.context.annotation.internalConfigurationAnnotationProcessor
 *                  String[] postProcessorNames =
 *                                         beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
 *                      invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
 *                          postProcessor.postProcessBeanDefinitionRegistry(registry);
 *                              processConfigBeanDefinitions(registry);
 *                                  this.reader.loadBeanDefinitions(configClasses);
 *
 * @author 华云龙
 * @date 2018-10-25
 */
@Configuration
@Import(value = {Computer.class, Cat.class, MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})
public class ImportConfig {
}
