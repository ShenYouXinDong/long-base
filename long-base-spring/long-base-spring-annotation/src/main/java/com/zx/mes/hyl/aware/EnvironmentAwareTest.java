package com.zx.mes.hyl.aware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * EnvironmentAware test
 *
 * @author 华云龙
 * @date 2019-1-9
 */
@Component
@Slf4j
public class EnvironmentAwareTest implements EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        log.debug("EnvironmentAware测试:"+environment);
        String[] activeProfiles = environment.getActiveProfiles();
        log.debug("--------------- 配置文件 --------------");
        for (String profile:activeProfiles) {
            log.debug("使用的配置文件:"+profile);
        }
    }
}
