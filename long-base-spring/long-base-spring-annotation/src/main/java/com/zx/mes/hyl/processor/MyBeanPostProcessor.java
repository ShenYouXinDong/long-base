package com.zx.mes.hyl.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * BeanPostProcessor impl bean后置处理器
 *
 * @author 华云龙
 * @date 2018-10-27
 */
public class MyBeanPostProcessor implements BeanPostProcessor{
    /**
     *
     * @param bean 当前要实例化的bean
     * @param beanName bean名称
     * @return 可返回包装的bean,也可原装返回
     * @throws BeansException beanException
     */
    @SuppressWarnings("NullableProblems")
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization:--->"+beanName);
        return bean;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization:--->"+beanName);
        return bean;
    }
}
