package com.zx.mes.hyl.dao;

import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Repository;

/**
 * dao autowired 用作自动装配测试
 *
 * @author 华云龙
 * @date 2018-10-27
 */
@Repository
@Setter
@ToString
@Accessors(chain = true)
public class BaseDao {
    private String label;

    public void test(){
        System.out.println("dao test! label:"+label);
    }
}
