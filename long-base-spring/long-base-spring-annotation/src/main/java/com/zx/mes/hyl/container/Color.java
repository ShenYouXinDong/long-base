package com.zx.mes.hyl.container;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.util.StringValueResolver;

/**
 * 1.自定义组件想要使用spring容器底层的一些组件如:(ApplicationContext,BeanFactory,xxx)
 * 2.自定义组件实现xxxAware,在创建对象的时候,会调用接口规定的方法注入相关组件
 * 3.xxxAware:功能使用xxxProcessor,如:ApplicationContextAware ---> ApplicationContextAwareProcessor
 *
 * @author 华云龙
 * @date 2018-10-29
 */
public class Color implements ApplicationContextAware,BeanFactoryAware,EmbeddedValueResolverAware,BeanNameAware {
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("beanFactory:"+beanFactory);
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("bean name:"+name);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("applicationContext:"+applicationContext);
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        System.out.println("stringValueResolver:"+resolver);
        String value = resolver.resolveStringValue("系统名称:${os.name} 已装年限:#{1*3}");
        System.out.println(value);
    }
}
