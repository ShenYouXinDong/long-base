package com.zx.mes.hyl.postprocess;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

/**
 * BeanFactoryPostProcessor
 *
 * @author 华云龙
 * @date 2019-2-1
 */
@Component
public class MyBeanFactoryPostProcess implements BeanFactoryPostProcessor{
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        System.out.println("-------------- MyBeanFactoryPostProcess -------------");
        Stream.of(beanDefinitionNames).forEach(System.out::println);
    }
}
