package com.zx.mes.hyl.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eureka 服务
 * @author 华云龙
 * @date 2018-6-23
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaApplication {

    public static void main(String[]args){
        SpringApplication.run(EurekaApplication.class, args);
    }
}
