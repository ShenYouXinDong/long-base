package com.zx.mes.hyl.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * pre网关过滤器 (取消)
 * @author hyl
 * @date 2017-12-17
 */
@Component
public class AccessFilter extends ZuulFilter {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        //做校验
        RequestContext ctx= RequestContext.getCurrentContext();
        final HttpServletRequest request = ctx.getRequest();
        logger.info("URI:"+request.getRequestURI());
        logger.info("URL:"+request.getRequestURL());


        Enumeration<String> headerNames = request.getHeaderNames();
        logger.info("解析出来的URI:[{}]",request.getRequestURI());
        while (headerNames.hasMoreElements()){
            logger.info(headerNames.nextElement().toString());
        }
        logger.info("authorization:第三方secret:[{}]",request.getHeader("authorization"));

        return null;
    }
}
