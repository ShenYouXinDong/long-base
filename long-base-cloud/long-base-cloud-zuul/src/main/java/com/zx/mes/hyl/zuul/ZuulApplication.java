package com.zx.mes.hyl.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * zuul 网关
 * @author huayunlong
 * @date 2018-6-23
 */
@SpringCloudApplication
@EnableZuulProxy
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class ZuulApplication {

    public static void main(String[]args){
        SpringApplication.run(ZuulApplication.class,args);
    }
}
